/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.pockets;

/**
 * @param <T>
 * @author Osvaldo Miguel Colin
 */
public class ThrowablePocket<T extends Throwable> extends Pocket<T> {

    public void throwIfPresent() throws T {
        if (!isEmpty()) {
            throw get();
        }
    }

    public void throwRuntimeExceptionIfPresent() {
        if (!isEmpty()) {
            Throwable throwable = get();

            if (throwable instanceof RuntimeException) {
                throw (RuntimeException) throwable;
            } else {
                throw new RuntimeException(throwable);
            }
        }
    }
}
