/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.pockets;

import java.util.Objects;

/**
 * @param <F>
 * @author Osvaldo Miguel Colin
 */
public class Pocket<F> {

    private volatile F value;

    public Pocket() {
        value = null;
    }

    public Pocket(F value) {
        this.value = value;
    }

    // SET
    public boolean isEmpty() {
        return value == null;
    }

    public F get() {
        return value;
    }

    public void set(F value) {
        this.value = value;
    }

    public void discard() {
        this.value = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pocket<?> pocket = (Pocket<?>) o;
        return Objects.equals(value, pocket.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    @Override
    public String toString() {
        return Objects.toString(value);
    }

}
