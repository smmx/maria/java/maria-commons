/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons;

import com.smmx.maria.commons.api.APIResponse;
import com.smmx.maria.commons.collections.utils.ListUtils;
import com.smmx.maria.commons.collections.utils.MapUtils;
import com.smmx.maria.commons.collections.Tuple;
import com.smmx.maria.commons.database.connections.ConnectionOperation;
import com.smmx.maria.commons.database.sql.DefaultSQLContext;
import com.smmx.maria.commons.database.sql.SQLTemplate;
import com.smmx.maria.commons.database.connections.Transaction;
import com.smmx.maria.commons.pockets.Pocket;
import com.smmx.maria.commons.time.CalendarUtils;
import com.smmx.maria.commons.values.processors.DictionaryProcessor;
import com.smmx.maria.commons.values.processors.ValueProcessor;
import com.smmx.maria.commons.values.interpreters.InterpreterManager;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.StringUtils;

import java.nio.file.Paths;
import java.util.*;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * @author Osvaldo Miguel Colin
 */
public class MariaCommons {

    // PATH
    public static String path(String base_dir, String... path) {
        return Paths.get(base_dir, path).toString();
    }

    // API
    public static APIResponse apiResult() {
        return new APIResponse();
    }

    public static APIResponse apiSuccess() {
        return apiResult()
            .status(200)
            .payload(dict("success", true));
    }

    public static APIResponse apiSuccess(int status) {
        return apiResult()
            .status(status)
            .payload(dict("success", true));
    }

    public static APIResponse apiSuccess(Map<String, Object> payload) {
        return apiResult()
            .status(200)
            .payload(payload);
    }

    public static APIResponse apiSuccess(int status, Map<String, Object> payload) {
        return apiResult()
            .status(status)
            .payload(payload);
    }

    // SQL
    public static SQLTemplate sql(String sql) {
        return DefaultSQLContext.getInstance().sql(sql);
    }

    public static Transaction transaction(ConnectionOperation operation) {
        return new Transaction(operation);
    }

    // ALGOS
    public static <T extends Comparable> boolean between(T value, T min, T max) {
        return min.compareTo(value) <= 0 && value.compareTo(max) <= 0;
    }

    public static <T extends Comparable> T max(Collection<T> values) {
        return maxMapped(v -> v, values);
    }

    public static <T extends Comparable> T max(T... values) {
        return maxMapped(v -> v, values);
    }

    public static <T, C extends Comparable> T maxMapped(Function<T, C> vf, Collection<T> values) {
        return values.stream()
            .filter(Objects::nonNull)
            .max((v1, v2) -> vf.apply(v1).compareTo(vf.apply(v2)))
            .get();
    }

    public static <T, C extends Comparable> T maxMapped(Function<T, C> vf, T... values) {
        Pocket<T> max_pocket = new Pocket<>();

        for (T value : values) {
            C cvalue = vf.apply(value);

            if (cvalue == null) {
                continue;
            }

            if (max_pocket.isEmpty()) {
                max_pocket.set(value);
            } else {
                C max = vf.apply(max_pocket.get());

                if (max.compareTo(cvalue) < 0) {
                    max_pocket.set(value);
                }
            }
        }

        return max_pocket.get();
    }

    public static <T extends Comparable> T min(Collection<T> values) {
        return minMapped(v -> v, values);
    }

    public static <T extends Comparable> T min(T... values) {
        return minMapped(v -> v, values);
    }

    public static <T, C extends Comparable> T minMapped(Function<T, C> vf, Collection<T> values) {
        return values.stream()
            .filter(Objects::nonNull)
            .min((v1, v2) -> vf.apply(v1).compareTo(vf.apply(v2)))
            .get();
    }

    public static <T, C extends Comparable> T minMapped(Function<T, C> vf, T... values) {
        Pocket<T> max_pocket = new Pocket<>();

        for (T value : values) {
            C cvalue = vf.apply(value);

            if (cvalue == null) {
                continue;
            }

            if (max_pocket.isEmpty()) {
                max_pocket.set(value);
            } else {
                C max = vf.apply(max_pocket.get());

                if (max.compareTo(cvalue) > 0) {
                    max_pocket.set(value);
                }
            }
        }

        return max_pocket.get();
    }

    public static <T extends Object> T ifnull(T value, T or_else) {
        return value != null ? value : or_else;
    }

    public static <T extends Object> T when(Supplier<Boolean> condition, T then, T or_else) {
        return condition.get() ? then : or_else;
    }

    // COLLECTIONS
    public static Tuple t(Object... items) {
        return new Tuple(items);
    }

    public static <T> T[] a(T... items) {
        return array(items);
    }

    public static <T> List<T> l(T... items) {
        return list(items);
    }

    public static <T> Map<String, Object> d(T... items) {
        return dict(items);
    }

    public static Tuple tuple(Object... items) {
        return new Tuple(items);
    }

    public static <T> T[] array(T... items) {
        return items;
    }

    public static <T> List<T> list(T... items) {
        return ListUtils.of(items);
    }

    public static <T> List<T> list(List<T> base, T... items) {
        return ListUtils.of(base, items);
    }

    public static <T> List<T> ulist(T... items) {
        return Collections.unmodifiableList(ListUtils.of(items));
    }

    public static <T> List<T> ulist(List<T> base, T... items) {
        return Collections.unmodifiableList(ListUtils.of(base, items));
    }

    public static Map<String, Object> dict(Object... items) {
        return MapUtils.of(items);
    }

    public static Map<String, Object> dict(Map<String, Object> base, Object... items) {
        return MapUtils.of(base, items);
    }

    public static Map<String, Object> udict(Object... items) {
        return Collections.unmodifiableMap(MariaCommons.dict(items));
    }

    public static Map<String, Object> udict(Map<String, Object> base, Object... items) {
        return Collections.unmodifiableMap(dict(base, items));
    }

    // STRINGS
    public static String sf(String format, Object... args) {
        return String.format(format, args);
    }

    public static String mlstr(String... strings) {
        return xmlstr(" ", strings);
    }

    public static String xmlstr(String line_separator, String... strings) {
        StringBuilder builder = new StringBuilder();

        boolean first = true;

        for (String astring : strings) {
            // NORMALIZE
            astring = StringUtils.trimToEmpty(astring);

            // NEW LINE
            if (first) {
                first = false;
            } else {
                builder.append(line_separator);
            }

            // APPEND
            builder.append(astring);
        }

        return builder.toString();
    }

    // VALUES
    public static <T> T anyCast(Object value, Class<T> clazz) {
        return InterpreterManager.getInstance().valueOf(value, clazz);
    }

    public static <T> T staticCast(Object value, Class<T> clazz) {
        // CHECKS
        if (value == null) {
            return null;
        }

        // CAST
        return (T) value;
    }

    public static ValueProcessor<Object, Object> vp(String blame) {
        return new ValueProcessor(blame, input -> input);
    }

    public static ValueProcessor<Object, Object> vp() {
        return new ValueProcessor(input -> input);
    }

    public static DictionaryProcessor<Map<String, ?>> dp(String blame) {
        return new DictionaryProcessor(blame, input -> {
            try {
                return input;
            } catch (ClassCastException ex) {
                if (input instanceof Map) {
                    return Collections.unmodifiableMap(
                        (Map<? extends String, ?>) input
                    );
                }

                throw ex;
            }
        });
    }

    public static DictionaryProcessor<Map<String, ?>> dp() {
        return new DictionaryProcessor(input -> input);
    }

    // DATA
    public static <T> T valueOf(Object value, Class<T> as_class) {
        return InterpreterManager.getInstance()
            .valueOf(value, as_class);
    }

    // DATES
    public static Date now() {
        return Calendar.getInstance().getTime();
    }

    public static Date date(String date) {
        return anyCast(date, Date.class);
    }

    public static Date today() {
        return CalendarUtils.toStartOfDay(Calendar.getInstance()).getTime();
    }

    // BIN
    public static byte[] hexToBin(String hex) {
        if (hex == null) {
            return null;
        }

        try {
            return Hex.decodeHex(hex);
        } catch (DecoderException ex) {
            throw new IllegalArgumentException(ex);
        }
    }

    public static String binToHex(byte[] bin) {
        if (bin == null) {
            return null;
        }

        return Hex.encodeHexString(bin);
    }

    // UUID
    public static String uuid() {
        return UUID.randomUUID().toString().replace("-", "").toUpperCase();
    }

    public static byte[] buuid() {
        return hexToBin(uuid());
    }
}
