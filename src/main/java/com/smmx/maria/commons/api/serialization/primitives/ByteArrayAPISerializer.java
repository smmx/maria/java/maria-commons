/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.api.serialization.primitives;

import com.smmx.maria.commons.api.serialization.APISerializer;

import static com.smmx.maria.commons.MariaCommons.binToHex;

/**
 * @author omiguelc
 */
public class ByteArrayAPISerializer extends APISerializer<byte[], String> {

    public ByteArrayAPISerializer() {
        super(byte[].class, String.class);
    }

    @Override
    public String serialize(byte[] value) {
        return binToHex(value);
    }

}
