/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.api;

import com.smmx.maria.commons.api.serialization.APISerializationManager;
import org.json.JSONObject;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Osvaldo Miguel Colin
 */
public class APIResponse {

    private int status;
    private final Map<String, Object> header;
    private final Map<String, Object> payload;

    public APIResponse() {
        this.status = 200;
        this.header = new HashMap<>();
        this.payload = new HashMap<>();
    }

    public int status() {
        return status;
    }

    public Map<String, Object> header() {
        return this.header;
    }

    public Map<String, Object> payload() {
        return this.payload;
    }

    public APIResponse status(int status) {
        this.status = status;
        return this;
    }

    public APIResponse header(String header, Object value) {
        this.header.put(header, value);
        return this;
    }

    public APIResponse payload(Map<String, Object> payload) {
        Map<String, Object> serialized = (Map<String, Object>) APISerializationManager.getInstance().serialize(payload);
        this.payload.putAll(serialized);
        return this;
    }

    public String toString() {
        Map<String, Object> response = new HashMap<>();

        response.put("header", header);
        response.put("payload", payload.isEmpty() ? Collections.emptyMap() : payload);

        return new JSONObject(response).toString();
    }

}
