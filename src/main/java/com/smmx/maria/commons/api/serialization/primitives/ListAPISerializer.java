/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.api.serialization.primitives;

import com.smmx.maria.commons.api.serialization.APISerializationManager;
import com.smmx.maria.commons.api.serialization.APISerializer;

import java.util.ArrayList;
import java.util.List;

/**
 * @author omiguelc
 */
public class ListAPISerializer extends APISerializer<List, List> {

    public ListAPISerializer() {
        super(List.class, List.class);
    }

    @Override
    public List serialize(List a_list) {
        // SHORT-CIRCUIT
        if (a_list == null) {
            return null;
        }

        // PROCESS
        List<Object> serialized_list = new ArrayList<>();

        a_list.forEach(value -> {
            // NORM VALUE
            if (value != null) {
                value = APISerializationManager.getInstance().serialize(value);
            }

            // RETURN
            serialized_list.add(value);
        });

        return serialized_list;
    }

}
