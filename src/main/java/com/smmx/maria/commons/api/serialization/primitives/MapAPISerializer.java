/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.api.serialization.primitives;

import com.smmx.maria.commons.api.serialization.APISerializationManager;
import com.smmx.maria.commons.api.serialization.APISerializer;

import java.util.HashMap;
import java.util.Map;

/**
 * @author omiguelc
 */
public class MapAPISerializer extends APISerializer<Map, Map> {

    public MapAPISerializer() {
        super(Map.class, Map.class);
    }

    @Override
    public Map serialize(Map a_map) {
        // SHORT-CIRCUIT
        if (a_map == null) {
            return null;
        }

        // PROCESS
        Map<String, Object> serialized_map = new HashMap<>();

        a_map.forEach((key, value) -> {
            // NORM KEY
            String norm_key = ((String) key).toLowerCase();

            // NORM VALUE
            if (value != null) {
                value = APISerializationManager.getInstance().serialize(value);
            }

            // RETURN
            serialized_map.put(norm_key, value);
        });

        return serialized_map;
    }

}
