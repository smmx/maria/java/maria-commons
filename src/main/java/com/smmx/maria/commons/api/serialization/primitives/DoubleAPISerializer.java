/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.api.serialization.primitives;

import com.smmx.maria.commons.api.serialization.APISerializer;

/**
 * @author Osvaldo Miguel Colin
 */
public class DoubleAPISerializer extends APISerializer<Double, Double> {

    public DoubleAPISerializer() {
        super(Double.class, Double.class);
    }

    @Override
    public Double serialize(Double value) {
        return value;
    }

}
