/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.api.serialization;

/**
 * @param <I>
 * @param <O>
 * @author Osvaldo Miguel Colin
 */
public abstract class APISerializer<I, O> {

    private final Class<I> iclazz;
    private final Class<O> oclazz;

    public APISerializer(Class<I> iclazz, Class<O> oclazz) {
        this.iclazz = iclazz;
        this.oclazz = oclazz;
    }

    public Class<I> getInputClass() {
        return iclazz;
    }

    public Class<O> getOutputClass() {
        return oclazz;
    }

    public abstract O serialize(I value);

}
