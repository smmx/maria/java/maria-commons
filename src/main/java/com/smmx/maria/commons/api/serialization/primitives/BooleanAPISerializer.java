/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.api.serialization.primitives;

import com.smmx.maria.commons.api.serialization.APISerializer;

/**
 * @author Osvaldo Miguel Colin
 */
public class BooleanAPISerializer extends APISerializer<Boolean, Boolean> {

    public BooleanAPISerializer() {
        super(Boolean.class, Boolean.class);
    }

    @Override
    public Boolean serialize(Boolean value) {
        return value;
    }

}
