/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.api.serialization.primitives;

import com.smmx.maria.commons.api.serialization.APISerializer;

/**
 * @author Osvaldo Miguel Colin
 */
public class LongAPISerializer extends APISerializer<Long, Long> {

    public LongAPISerializer() {
        super(Long.class, Long.class);
    }

    @Override
    public Long serialize(Long value) {
        return value;
    }

}
