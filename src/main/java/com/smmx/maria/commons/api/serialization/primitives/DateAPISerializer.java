
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.api.serialization.primitives;

import com.smmx.maria.commons.api.serialization.APISerializer;

import java.util.Date;

/**
 * @author Osvaldo Miguel Colin
 */
public class DateAPISerializer extends APISerializer<Date, Date> {

    public DateAPISerializer() {
        super(Date.class, Date.class);
    }

    @Override
    public Date serialize(Date value) {
        return value;
    }

}
