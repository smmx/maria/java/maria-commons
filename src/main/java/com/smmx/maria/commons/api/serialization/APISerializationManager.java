/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.api.serialization;

import com.smmx.maria.commons.api.serialization.primitives.*;
import com.smmx.maria.commons.values.interpreters.InterpreterUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * @author Osvaldo Miguel Colin
 */
public class APISerializationManager {

    // STATIC
    private static APISerializationManager INSTANCE;

    static {
        INSTANCE = null;
    }

    public static APISerializationManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new APISerializationManager();
        }

        return INSTANCE;
    }

    // MEMBER
    private final Map<Class<?>, APISerializer> serializers;

    private APISerializationManager() {
        // INIT
        serializers = new HashMap<>();

        // REGISTER
        register(new BooleanAPISerializer());
        register(new ByteArrayAPISerializer());
        register(new IntegerAPISerializer());
        register(new FloatAPISerializer());
        register(new DoubleAPISerializer());
        register(new LongAPISerializer());
        register(new BigDecimalAPISerializer());
        register(new StringAPISerializer());
        register(new DateAPISerializer());
        register(new ListAPISerializer());
        register(new MapAPISerializer());
    }

    public void register(APISerializer serializer) {
        serializers.put(serializer.getInputClass(), serializer);
    }

    public boolean has(Class<?> clazz) {
        Class<?> norm_clazz = InterpreterUtils.normalizeClass(clazz);

        // RETURN
        return serializers.containsKey(norm_clazz);
    }

    public <T> APISerializer<T, ?> get(Class<T> clazz) {
        Class<?> norm_clazz = InterpreterUtils.normalizeClass(clazz);

        // HANDLER
        APISerializer<T, ?> serializer = serializers.get(norm_clazz);

        // VALIDATE
        if (serializer == null) {
            throw new NoSuchElementException(clazz.getName());
        }

        return serializer;
    }

    public <T> Class<?> getOutputClass(Class<T> clazz) {
        return get(clazz).getOutputClass();
    }

    public Object serialize(Object value) {
        // NULL CHECK
        if (value == null) {
            return null;
        }

        // SERIALIZE
        Class<?> norm_clazz = InterpreterUtils.normalizeClass(value.getClass());

        // HANDLER
        APISerializer exact = serializers.get(norm_clazz);

        // SHORT-CIRCUIT
        if (exact != null) {
            return exact.serialize(value);
        }

        // FIND FIRST
        for (APISerializer candidate : serializers.values()) {
            if (candidate.getInputClass().isInstance(value)) {
                return candidate.serialize(value);
            }
        }

        return value;
    }

}
