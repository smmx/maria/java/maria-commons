/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.api.serialization.primitives;

import com.smmx.maria.commons.api.serialization.APISerializer;

import java.math.BigDecimal;

/**
 * @author Osvaldo Miguel Colin
 */
public class BigDecimalAPISerializer extends APISerializer<BigDecimal, BigDecimal> {

    public BigDecimalAPISerializer() {
        super(BigDecimal.class, BigDecimal.class);
    }

    @Override
    public BigDecimal serialize(BigDecimal value) {
        return value;
    }

}
