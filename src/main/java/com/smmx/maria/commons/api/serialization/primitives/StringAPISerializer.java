/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.api.serialization.primitives;

import com.smmx.maria.commons.api.serialization.APISerializer;

/**
 * @author Osvaldo Miguel Colin
 */
public class StringAPISerializer extends APISerializer<String, String> {

    public StringAPISerializer() {
        super(String.class, String.class);
    }

    @Override
    public String serialize(String value) {
        return value;
    }

}
