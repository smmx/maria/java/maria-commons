/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.images;

import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

import static com.smmx.maria.commons.MariaCommons.min;

/**
 * @author Osvaldo Miguel Colin
 */
public class ImageUtils {

    public static BufferedImage scale(BufferedImage before, int width, int height) {
        float bwidth = before.getWidth();
        float bheight = before.getHeight();
        float scale = min(
            width / bwidth,
            height / bheight
        );

        AffineTransform at
            = AffineTransform.getScaleInstance(
            scale,
            scale
        );

        AffineTransformOp scaleOp
            = new AffineTransformOp(
            at,
            AffineTransformOp.TYPE_BICUBIC
        );

        // NEW DIMENTIONS
        int nwidth = (int) (bwidth * scale);
        int nheight = (int) (bheight * scale);

        // SCALE
        BufferedImage scaled = new BufferedImage(
            nwidth,
            nheight,
            before.getType()
        );

        scaleOp.filter(before, scaled);

        // RETURN
        return scaled;
    }

    public static BufferedImage scaleForceDimensions(BufferedImage before, int width, int height) {
        float bwidth = before.getWidth();
        float bheight = before.getHeight();
        float scale = min(
            width / bwidth,
            height / bheight
        );

        AffineTransform at
            = AffineTransform.getScaleInstance(
            scale,
            scale
        );

        AffineTransformOp scaleOp
            = new AffineTransformOp(
            at,
            AffineTransformOp.TYPE_BICUBIC
        );

        // SCALE
        BufferedImage scaled = new BufferedImage(
            width,
            height,
            before.getType()
        );

        scaleOp.filter(before, scaled);

        // RENDER
        BufferedImage result = new BufferedImage(
            width,
            height,
            before.getType()
        );

        Graphics2D g = result.createGraphics();
        g.translate(
            (width - bwidth * scale) / 2,
            (height - bheight * scale) / 2
        );
        g.drawRenderedImage(scaled, new AffineTransform());
        g.dispose();

        // RETURN
        return result;
    }

}
