/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.optional;

/**
 * @author Osvaldo Miguel Colin
 */
public class UndefinedValue {

    private static UndefinedValue INSTANCE;

    static {
        INSTANCE = null;
    }

    public static UndefinedValue getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new UndefinedValue();
        }

        return INSTANCE;
    }

    private UndefinedValue() {
        // DO NOTHING
    }

}
