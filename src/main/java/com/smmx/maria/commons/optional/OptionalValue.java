/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.optional;

import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static com.smmx.maria.commons.MariaCommons.sf;

/**
 * @param <T>
 * @author Osvaldo Miguel Colin
 */
public class OptionalValue<T> {

    // STATIC
    private static final OptionalValue<?> UNDEFINED = new OptionalValue<>();

    public static <T> OptionalValue<T> undefined() {
        @SuppressWarnings("unchecked")
        OptionalValue<T> t = (OptionalValue<T>) UNDEFINED;
        return t;
    }

    public static <T> OptionalValue<T> of(T value) {
        return new OptionalValue<>(value);
    }

    // CLASS
    private final boolean is_present;
    private final T value;

    private OptionalValue() {
        this.is_present = false;
        this.value = null;
    }

    private OptionalValue(T value) {
        this.is_present = true;
        this.value = value;
    }

    public T get() {
        if (!is_present) {
            throw new NoSuchElementException("No value present");
        }
        return value;
    }

    public boolean isPresent() {
        return is_present;
    }

    public void ifPresent(Consumer<? super T> consumer) {
        if (is_present) {
            consumer.accept(value);
        }
    }

    public OptionalValue<T> filter(Predicate<? super T> predicate) {
        Objects.requireNonNull(predicate);

        if (!isPresent()) {
            return this;
        } else {
            return predicate.test(value) ? this : undefined();
        }
    }

    public <U> OptionalValue<U> map(Function<? super T, ? extends U> mapper) {
        Objects.requireNonNull(mapper);

        if (!isPresent()) {
            return undefined();
        } else {
            return OptionalValue.of(mapper.apply(value));
        }
    }

    public <U> OptionalValue<U> flatMap(Function<? super T, OptionalValue<U>> mapper) {
        Objects.requireNonNull(mapper);

        if (!isPresent()) {
            return undefined();
        } else {
            return mapper.apply(value);
        }
    }

    public T orElse(T other) {
        return is_present ? value : other;
    }

    public T orElseGet(Supplier<? extends T> other) {
        return is_present ? value : other.get();
    }

    public Object orElseUndefined() {
        return is_present ? value : UndefinedValue.getInstance();
    }

    public <X extends Throwable> T orElseThrow(Supplier<? extends X> exceptionSupplier) throws X {
        if (is_present) {
            return value;
        } else {
            throw exceptionSupplier.get();
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (!(obj instanceof OptionalValue)) {
            return false;
        }

        OptionalValue<?> other = (OptionalValue<?>) obj;
        return Objects.equals(value, other.value);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(value);
    }

    @Override
    public String toString() {
        return is_present
            ? sf("EOptional[%s]", value)
            : "EOptional.empty";
    }

}
