/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.serialization.primitives;

import com.smmx.maria.commons.database.serialization.SQLSerializer;

/**
 * @author Osvaldo Miguel Colin
 */
public class ByteArraySQLSerializer extends SQLSerializer<byte[], byte[]> {

    public ByteArraySQLSerializer() {
        super(byte[].class, byte[].class);
    }

    @Override
    public byte[] outgoing(byte[] value) {
        return value;
    }

}
