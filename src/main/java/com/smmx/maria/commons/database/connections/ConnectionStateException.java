/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.connections;

/**
 * @author Osvaldo Miguel Colin
 */
public class ConnectionStateException extends RuntimeException {

    public ConnectionStateException(String message) {
        super(message);
    }

}
