/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.serialization.primitives;

import com.smmx.maria.commons.database.serialization.SQLSerializer;

import java.sql.Blob;

/**
 * @author Osvaldo Miguel Colin
 */
public class SQLBlobSQLSerializer extends SQLSerializer<Blob, Blob> {

    public SQLBlobSQLSerializer() {
        super(Blob.class, Blob.class);
    }

    @Override
    public Blob outgoing(Blob value) {
        return value;
    }

}
