/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.resultsets;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Osvaldo Miguel Colin
 */
public class ResultSetUtils {

    public static int getFieldCount(ResultSet result_set) throws SQLException {
        return result_set.getMetaData().getColumnCount();
    }

    public static String[] getFieldNames(ResultSet result_set) throws SQLException {
        int field_count = result_set.getMetaData().getColumnCount();

        String[] fields = new String[field_count];

        for (int i = 0; i < field_count; i++) {
            fields[i] = result_set.getMetaData().getColumnLabel(i + 1);
        }

        return fields;
    }
}
