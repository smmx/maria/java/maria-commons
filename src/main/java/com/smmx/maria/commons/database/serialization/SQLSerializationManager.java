/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.serialization;

import com.smmx.maria.commons.database.serialization.primitives.*;
import com.smmx.maria.commons.values.interpreters.InterpreterUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * @author Osvaldo Miguel Colin
 */
public class SQLSerializationManager {

    // STATIC
    private static SQLSerializationManager INSTANCE;

    static {
        INSTANCE = null;
    }

    public static SQLSerializationManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new SQLSerializationManager();
        }

        return INSTANCE;
    }

    // MEMBER
    private final Map<Class<?>, SQLSerializer> handlers;

    private SQLSerializationManager() {
        // INIT
        handlers = new HashMap<>();

        // REGISTER
        register(new ObjectSQLSerializer());
        register(new BooleanSQLSerializer());
        register(new ByteArraySQLSerializer());
        register(new IntegerSQLSerializer());
        register(new FloatSQLSerializer());
        register(new DoubleSQLSerializer());
        register(new LongSQLSerializer());
        register(new BigDecimalSQLSerializer());
        register(new StringSQLSerializer());
        register(new ListSQLSerializer());
        register(new MapSQLSerializer());
        register(new DateSQLSerializer());
        register(new SQLBlobSQLSerializer());
        register(new SQLClobSQLSerializer());
        register(new SQLNClobSQLSerializer());
        register(new SQLDateSQLSerializer());
        register(new SQLTimestampSQLSerializer());
    }

    public void register(SQLSerializer handler) {
        handlers.put(handler.getIncomingClass(), handler);
    }

    public boolean isRegistered(Class<?> clazz) {
        Class<?> norm_clazz = InterpreterUtils.normalizeClass(clazz);

        // RETURN
        return handlers.containsKey(norm_clazz);
    }

    public <T> SQLSerializer<T, ?> handler(Class<T> clazz) {
        Class<?> norm_clazz = InterpreterUtils.normalizeClass(clazz);

        // HANDLER
        SQLSerializer<T, ?> handler = handlers.get(norm_clazz);

        // VALIDATE
        if (handler == null) {
            throw new NoSuchElementException(clazz.getName());
        }

        return handler;
    }

    public <T> T incoming(Object value, Class<T> clazz) {
        // NULL CHECK
        if (value == null) {
            return null;
        }

        // TYPE CHECK
        if (clazz.isInstance(value)) {
            return (T) value;
        }

        // VALUE
        return handler(clazz).incoming(value);
    }

    public <T> Object outgoing(Object value, Class<T> clazz) {
        // NULL CHECK
        if (value == null) {
            return null;
        }

        // HANDLER
        SQLSerializer<T, ?> handler = handler(clazz);

        // CAST
        if (handler.getOutgoingClass().isInstance(value)) {
            return value;
        } else {
            if (handler.getIncomingClass().isInstance(value)) {
                return handler.outgoing((T) value);
            }

            return handler.outgoing(
                handler.incoming(value)
            );
        }
    }
}
