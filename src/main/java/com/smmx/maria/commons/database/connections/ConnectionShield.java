package com.smmx.maria.commons.database.connections;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.Map;
import java.util.Properties;

public class ConnectionShield {

    // STATIC
    private static final long MIN_VALIDATION_PERIOD = 1800000; // 30 MINUTES

    private static final Logger LOGGER = LoggerFactory.getLogger(ConnectionShield.class);

    private enum ConnectionState {
        VIRGIN, CONNECTED, INVALID
    }

    // MEMBER
    private final String url;
    private final Properties properties;

    private ConnectionState connection_state;
    private Connection connection;
    private ConnectionTransactionHandler transaction_handler;

    private long last_validation_timestamp;
    private boolean last_validation_result;

    public ConnectionShield(String url, Map<String, Object> properties) {
        this.url = url;

        this.properties = new Properties();
        this.properties.putAll(properties);

        this.connection_state = ConnectionState.VIRGIN;
        this.connection = null;
        this.transaction_handler = null;

        this.last_validation_timestamp = 0;
        this.last_validation_result = false;
    }

    public String getURL() {
        return url;
    }

    private void invalidate() {
        connection_state = ConnectionState.INVALID;
        connection = null;
        transaction_handler = null;
    }

    // WRAPPERS
    public Connection getConnection() {
        // SHORT-CIRCUIT
        if (connection_state.equals(ConnectionState.INVALID)) {
            throw new ConnectionStateException("Connection shield is invalid.");
        }

        // OPEN CONNECTION
        if (connection_state.equals(ConnectionState.VIRGIN)) {
            try {
                // CREATE CONNECTION
                connection = DriverManager.getConnection(url, properties);
                connection.setAutoCommit(true);
                connection_state = ConnectionState.CONNECTED;

                // SET VALID
                last_validation_timestamp = System.currentTimeMillis();
                last_validation_result = true;
            } catch (SQLException ex) {
                LOGGER.error("Failed to connect.", ex);

                // INVALIDATE
                this.invalidate();
            }
        }

        // RETURN
        return connection;
    }

    public boolean isValid(int timeout) {
        // SHORT-CIRCUIT
        if (connection_state.equals(ConnectionState.INVALID)) {
            return false;
        }

        // TIMESTAMP
        long timestamp = System.currentTimeMillis();
        long tdelta = timestamp - last_validation_timestamp;

        if (tdelta < MIN_VALIDATION_PERIOD) {
            return last_validation_result;
        }

        // VALIDATE
        boolean result;

        try {
            result = getConnection().isValid(timeout);
        } catch (SQLException ex) {
            result = false;
        }

        if (!result) {
            this.invalidate();
        }

        // UPDATE LAST VALIDATION
        last_validation_timestamp = timestamp;
        last_validation_result = result;

        // RETURN
        return result;
    }

    public void commit() throws SQLException {
        try {
            getConnection().commit();
        } catch (SQLException ex) {
            // INVALIDATE
            this.invalidate();

            // RETURN
            throw ex;
        }
    }

    public void rollback() throws SQLException {
        try {
            getConnection().rollback();
        } catch (SQLException ex) {
            // INVALIDATE
            this.invalidate();

            // RETURN
            throw ex;
        }
    }

    public void close() throws SQLException {
        // SHORT-CIRCUIT
        if (connection_state.equals(ConnectionState.INVALID)) {
            return;
        }

        // CLOSE
        if (connection_state.equals(ConnectionState.CONNECTED)) {
            // CLOSE TRANSACTION
            if (transaction_handler != null) {
                transaction_handler.close(false);
            }

            // CLOSE CONNECTION
            try {
                connection.close();
            } catch (SQLException ex) {
                LOGGER.error("Failed to close connection.", ex);
            }
        }

        // INVALIDATE
        this.invalidate();
    }

    // STATEMENTS
    public Statement createStatement() throws SQLException {
        return getConnection()
            .createStatement(ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
    }

    public PreparedStatement prepareStatement(String sql) throws SQLException {
        return getConnection()
            .prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
    }

    public CallableStatement prepareCall(String sql) throws SQLException {
        return getConnection()
            .prepareCall(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
    }

    // TRANSACTION
    public boolean isTransactionOpen() {
        return transaction_handler != null;
    }

    public ConnectionTransactionHandler getTransactionHandler() {
        return transaction_handler;
    }

    public ConnectionTransactionHandler openTransaction() throws SQLException {
        // SHORT-CIRCUIT
        if (transaction_handler != null) {
            return null;
        }

        try {
            // NEW INSTANCE
            transaction_handler = new ConnectionTransactionHandler();

            // RETURN
            return transaction_handler;
        } catch (SQLException ex) {
            throw ex;
        }
    }

    public class ConnectionTransactionHandler {

        private ConnectionTransactionHandler() throws SQLException {
            transaction_handler = this;

            try {
                getConnection().setAutoCommit(false);
            } catch (SQLException ex) {
                // INVALIDATE
                ConnectionShield.this.invalidate();

                // RETURN
                throw ex;
            }
        }

        public void close(boolean commit) throws SQLException {
            if (transaction_handler != this) {
                throw new ConnectionTransactionStateException("Transaction has been closed.");
            }

            // REMOVE TRANSACTION
            transaction_handler = null;

            // COMMIT/ROLLBACK
            SQLException exception = null;

            try {
                if (commit) {
                    getConnection().commit();
                } else {
                    getConnection().rollback();
                }
            } catch (SQLException ex) {
                exception = ex;
            }

            // ENABLE AUTOCOMMIT
            try {
                getConnection().setAutoCommit(true);
            } catch (SQLException ex) {
                exception = ex;
            }

            // INVALIDATE
            if (exception != null) {
                ConnectionShield.this.invalidate();
                throw exception;
            }
        }

    }

}
