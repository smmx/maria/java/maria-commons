/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.serialization.primitives;

import com.smmx.maria.commons.database.serialization.SQLSerializer;
import com.smmx.maria.commons.values.interpreters.InterpreterManager;

import java.util.Date;

/**
 * @author Osvaldo Miguel Colin
 */
public class SQLDateSQLSerializer extends SQLSerializer<java.sql.Date, java.sql.Date> {

    public SQLDateSQLSerializer() {
        super(java.sql.Date.class, java.sql.Date.class);
    }

    @Override
    public java.sql.Date incoming(Object value) {
        Date date = InterpreterManager.getInstance()
            .valueOf(value, Date.class);

        return new java.sql.Date(date.getTime());
    }

    @Override
    public java.sql.Date outgoing(java.sql.Date value) {
        return value;
    }

}
