/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.resultsets;

import com.smmx.maria.commons.database.serialization.SQLSerializationManager;
import com.smmx.maria.commons.pockets.Pocket;
import com.smmx.maria.commons.pockets.ThrowablePocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * @author Osvaldo Miguel Colin
 */
public class ResultSetIterator implements Iterator<List> {

    // STATIC
    private static final Logger LOGGER = LoggerFactory.getLogger(ResultSetIterator.class);

    // MEMBER
    private final ResultSet result_set;

    private final Class<?>[] datatypes;
    private final int column_count;

    private final Runnable close_handler;
    private final ThrowablePocket<SQLException> exception_pocket;

    // STATE
    private boolean closed;
    private boolean record_loaded;
    private boolean fatal_exception;

    public ResultSetIterator(ResultSet result_set, Class<?>[] datatypes, Runnable close_handler, ThrowablePocket<SQLException> exception_pocket) {
        this.result_set = result_set;

        this.datatypes = datatypes;
        this.column_count = datatypes.length;

        this.close_handler = close_handler;
        this.exception_pocket = exception_pocket;

        // INITIAL STATE
        closed = false;
        record_loaded = false;
        fatal_exception = false;

        exception_pocket.discard();
    }

    @Override
    public boolean hasNext() {
        return loadRecord();
    }

    @Override
    public List next() {
        if (loadRecord()) {
            List row = new ArrayList<>();

            try {
                record_loaded = false;

                for (int i = 0; i < column_count; i++) {
                    Class<?> datatype = datatypes[i];
                    Object value = result_set.getObject(i + 1);

                    value = SQLSerializationManager.getInstance()
                        .incoming(value, datatype);

                    row.add(value);
                }
            } catch (SQLException ex) {
                exception_pocket.set(ex);
                fatal_exception = true;
            }

            // CAN CONTINUE?
            if (fatal_exception) {
                // CLOSE
                if (!closed) {
                    closed = true;

                    // TRY CLOSE
                    try {
                        close_handler.run();
                    } catch (Exception ex) {
                        LOGGER.error("Unhandled exception.", ex);
                    }
                }

                // RETURN
                return null;
            }

            // RETURN RECORD
            return Collections.unmodifiableList(row);
        } else {
            throw new IllegalStateException("ResultSet has no more values to iterate");
        }
    }

    private boolean loadRecord() {
        if (!record_loaded) {
            try {
                // CHECKS
                if (result_set.isClosed()) {
                    return false;
                }

                if (fatal_exception) {
                    return false;
                }

                // LOAD
                record_loaded = result_set.next();
            } catch (SQLException ex) {
                exception_pocket.set(ex);
                fatal_exception = true;
            }
        }

        // CAN CONTINUE?
        if (fatal_exception || !record_loaded) {
            // CLOSE
            if (!closed) {
                closed = true;

                // TRY CLOSE
                try {
                    close_handler.run();
                } catch (Exception ex) {
                    LOGGER.error("Unhandled exception.", ex);
                }
            }

            // STOP
            return false;
        }

        // CONTINUE
        return true;
    }
}
