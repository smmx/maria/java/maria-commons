/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.sql;

/**
 * @author Osvaldo Miguel Colin
 */
public class DefaultSQLContext extends SQLContext {

    // STATIC
    private static DefaultSQLContext INSTANCE;

    static {
        INSTANCE = null;
    }

    public static DefaultSQLContext getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new DefaultSQLContext();
        }

        return INSTANCE;
    }

    // CLASS
    private DefaultSQLContext() {
        super();
    }

}
