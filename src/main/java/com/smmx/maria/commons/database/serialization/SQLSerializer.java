/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.serialization;

import com.smmx.maria.commons.values.interpreters.InterpreterManager;

/**
 * @param <I>
 * @param <O>
 * @author Osvaldo Miguel Colin
 */
public abstract class SQLSerializer<I, O> {

    private final Class<I> iclazz;
    private final Class<O> oclazz;

    public SQLSerializer(Class<I> iclazz, Class<O> oclazz) {
        this.iclazz = iclazz;
        this.oclazz = oclazz;
    }

    public Class<I> getIncomingClass() {
        return iclazz;
    }

    public Class<O> getOutgoingClass() {
        return oclazz;
    }

    public I incoming(Object value) {
        return InterpreterManager.getInstance()
            .valueOf(value, iclazz);
    }

    public abstract O outgoing(I value);

}
