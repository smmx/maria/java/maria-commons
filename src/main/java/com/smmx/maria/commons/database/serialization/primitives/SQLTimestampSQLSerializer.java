/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.serialization.primitives;

import com.smmx.maria.commons.database.serialization.SQLSerializer;
import com.smmx.maria.commons.values.interpreters.InterpreterManager;

import java.sql.Timestamp;
import java.util.Date;

/**
 * @author Osvaldo Miguel Colin
 */
public class SQLTimestampSQLSerializer extends SQLSerializer<Timestamp, Timestamp> {

    public SQLTimestampSQLSerializer() {
        super(java.sql.Timestamp.class, java.sql.Timestamp.class);
    }

    @Override
    public Timestamp incoming(Object value) {
        Date date = InterpreterManager.getInstance()
            .valueOf(value, Date.class);

        return new java.sql.Timestamp(date.getTime());
    }

    @Override
    public Timestamp outgoing(Timestamp value) {
        return value;
    }

}
