/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.connections;

import com.smmx.maria.commons.database.connections.ConnectionShield.ConnectionTransactionHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Osvaldo Miguel Colin
 */
public class Transaction implements ConnectionOperation {

    // STATIC
    private static final Logger LOGGER = LoggerFactory.getLogger(Transaction.class);

    // MEMBER
    private final ConnectionOperation operation;
    private final List<ConnectionOperation> run_on_close;

    public Transaction(ConnectionOperation operation) {
        this.operation = operation;
        this.run_on_close = new ArrayList<>();
    }

    public Transaction onClose(ConnectionOperation operation) {
        // ON CLOSE
        this.run_on_close.add(operation);

        // NEW TRANSACTION
        return this;
    }

    @Override
    public void run(ConnectionShield connection) throws SQLException {
        // CREATE EXCEPTION POINTER
        Throwable throwable = null;

        // OPEN TRANSACTION
        ConnectionTransactionHandler transaction_handler = connection.openTransaction();

        try {
            operation.run(connection);
        } catch (Throwable ex) {
            throwable = ex;
        } finally {
            // END TRANSACTION
            if (transaction_handler != null) {
                try {
                    transaction_handler.close(throwable == null);
                } catch (SQLException ex) {
                    LOGGER.error("Failed to end transaction.", ex);
                }
            } else {
                LOGGER.warn("Nested transaction found, this should be avoided since it can cause unstable states in the database.");
            }

            // ON CLOSE
            run_on_close.forEach(operation -> {
                try {
                    operation.run(connection);
                } catch (SQLException ex) {
                    LOGGER.error("On close operation failed; ignoring.", ex);
                }
            });
        }

        // PROPAGATE ERROR
        if (throwable != null) {
            if (throwable instanceof SQLException) {
                throw (SQLException) throwable;
            } else if (throwable instanceof RuntimeException) {
                throw (RuntimeException) throwable;
            } else {
                LOGGER.error("Transaction error.", throwable);
            }
        }
    }
}
