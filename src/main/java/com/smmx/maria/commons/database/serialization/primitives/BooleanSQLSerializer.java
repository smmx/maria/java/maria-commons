/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.serialization.primitives;

import com.smmx.maria.commons.database.serialization.SQLSerializer;

/**
 * @author Osvaldo Miguel Colin
 */
public class BooleanSQLSerializer extends SQLSerializer<Boolean, Boolean> {

    public BooleanSQLSerializer() {
        super(Boolean.class, Boolean.class);
    }

    @Override
    public Boolean outgoing(Boolean value) {
        return value;
    }

}
