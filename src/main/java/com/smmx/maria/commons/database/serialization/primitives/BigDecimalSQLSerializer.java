/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.serialization.primitives;

import com.smmx.maria.commons.database.serialization.SQLSerializer;

import java.math.BigDecimal;

/**
 * @author Osvaldo Miguel Colin
 */
public class BigDecimalSQLSerializer extends SQLSerializer<BigDecimal, BigDecimal> {

    public BigDecimalSQLSerializer() {
        super(BigDecimal.class, BigDecimal.class);
    }

    @Override
    public BigDecimal outgoing(BigDecimal value) {
        return value;
    }

}
