/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.sql;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Osvaldo Miguel Colin
 */
public class SQLTemplate extends SQL {

    // STATIC
    private static final Pattern TAG_REGEX = Pattern.compile("\\{(%|\\?)\\s([\\w-]+)\\s\\1\\}");

    // MEMBER
    private final Map<String, SQL> definitions;

    public SQLTemplate(String sql) {
        super(sql);

        this.definitions = new HashMap<>();
    }

    // TEMPLATE
    @Override
    public SQLTemplate registerField(String name, Class<?> datatype) {
        return (SQLTemplate) super.registerField(name, datatype);
    }

    @Override
    public SQLTemplate registerParameter(String name, Class<?> datatype) {
        return (SQLTemplate) super.registerParameter(name, datatype);
    }

    @Override
    public SQLTemplate registerFields(Map<String, Class<?>> fields) {
        return (SQLTemplate) super.registerFields(fields);
    }

    @Override
    public SQLTemplate registerParameters(Map<String, Class<?>> parameters) {
        return (SQLTemplate) super.registerParameters(parameters);
    }

    public SQLTemplate define(String name, SQL snippet) {
        definitions.put(name, snippet);
        return this;
    }

    public SQLTemplate define(Map<String, SQL> snippets) {
        this.definitions.putAll(snippets);
        return this;
    }

    public SQL render() {
        // INIT
        StringBuffer rendered_sql_sb = new StringBuffer();
        Map<String, Class<?>> all_fields = new HashMap(getFields());
        Map<String, Class<?>> all_parameters = new HashMap(getParameters());

        // MATCH
        Matcher matcher = TAG_REGEX.matcher(getSQL());

        while (matcher.find()) {
            boolean required = "%".equals(matcher.group(1));
            String definition_name = matcher.group(2);

            // RESOLVE DEFINITION
            SQL snippet = null;

            if (definitions.containsKey(definition_name)) {
                snippet = definitions.get(definition_name);
            }

            // INJECT DEFINITION
            if (snippet != null) {
                // RENDER DEFINITION
                if (snippet instanceof SQLTemplate) {
                    // CLONE DEFINITIONS
                    Map<String, SQL> template_definitions = new HashMap(definitions);

                    // AVOID RECURSIVE REFERENCE
                    template_definitions.remove(definition_name);

                    // CAST
                    SQLTemplate template_snippet = (SQLTemplate) snippet;

                    // INHERIT DEFINITIONS
                    template_snippet.define(template_definitions);

                    // RENDER CHILD
                    snippet = template_snippet.render();
                }

                // INJECT
                matcher.appendReplacement(rendered_sql_sb, snippet.getSQL());

                // APPEND
                snippet.getFields().forEach(all_fields::putIfAbsent);
                snippet.getParameters().forEach(all_parameters::putIfAbsent);
            } else if (required) {
                throw new SQLTemplateException("Required definition is missing: " + definition_name + ".");
            } else {
                matcher.appendReplacement(rendered_sql_sb, "");
            }
        }

        matcher.appendTail(rendered_sql_sb);

        // RENDER
        String rendered_sql = rendered_sql_sb.toString();

        // RETURN
        return new SQL(rendered_sql)
            .registerFields(all_fields)
            .registerParameters(all_parameters);
    }

    @Override
    public PreparedStatementFactory build() {
        return render().build();
    }

}
