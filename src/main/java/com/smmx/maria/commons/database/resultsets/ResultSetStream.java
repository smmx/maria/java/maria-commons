/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.resultsets;

import com.smmx.maria.commons.pockets.ThrowablePocket;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.Iterator;
import java.util.Optional;
import java.util.Spliterator;
import java.util.function.*;
import java.util.stream.*;

/**
 * @param <T>
 * @author Osvaldo Miguel Colin
 */
public class ResultSetStream<T> {

    private final Stream<T> stream;
    private final ThrowablePocket<SQLException> exception_pocket;

    public ResultSetStream(Stream<T> stream, ThrowablePocket<SQLException> exception_pocket) {
        this.stream = stream;
        this.exception_pocket = exception_pocket;
    }

    // STREAM
    public Stream<T> filter(Predicate<? super T> predicate) {
        return stream.filter(predicate);
    }

    public <R> Stream<R> map(Function<? super T, ? extends R> mapper) {
        return stream.map(mapper);
    }

    public IntStream mapToInt(ToIntFunction<? super T> mapper) {
        return stream.mapToInt(mapper);
    }

    public LongStream mapToLong(ToLongFunction<? super T> mapper) {
        return stream.mapToLong(mapper);
    }

    public DoubleStream mapToDouble(ToDoubleFunction<? super T> mapper) {
        return stream.mapToDouble(mapper);
    }

    public <R> Stream<R> flatMap(Function<? super T, ? extends Stream<? extends R>> mapper) {
        return stream.flatMap(mapper);
    }

    public IntStream flatMapToInt(Function<? super T, ? extends IntStream> mapper) {
        return stream.flatMapToInt(mapper);
    }

    public LongStream flatMapToLong(Function<? super T, ? extends LongStream> mapper) {
        return stream.flatMapToLong(mapper);
    }

    public DoubleStream flatMapToDouble(Function<? super T, ? extends DoubleStream> mapper) {
        return stream.flatMapToDouble(mapper);
    }

    public Stream<T> distinct() {
        return stream.distinct();
    }

    public Stream<T> sorted() {
        return stream.sorted();
    }

    public Stream<T> sorted(Comparator<? super T> comparator) {
        return stream.sorted(comparator);
    }

    public Stream<T> peek(Consumer<? super T> action) {
        return stream.peek(action);
    }

    public Stream<T> limit(long maxSize) {
        return stream.limit(maxSize);
    }

    public Stream<T> skip(long n) {
        return stream.skip(n);
    }

    public void forEach(Consumer<? super T> action) {
        stream.forEach(action);
    }

    public void forEachOrdered(Consumer<? super T> action) {
        stream.forEachOrdered(action);
    }

    public Object[] toArray() {
        return stream.toArray();
    }

    public <A> A[] toArray(IntFunction<A[]> generator) {
        return stream.toArray(generator);
    }

    public T reduce(T identity, BinaryOperator<T> accumulator) {
        return stream.reduce(identity, accumulator);
    }

    public Optional<T> reduce(BinaryOperator<T> accumulator) {
        return stream.reduce(accumulator);
    }

    public <U> U reduce(U identity, BiFunction<U, ? super T, U> accumulator, BinaryOperator<U> combiner) {
        return stream.reduce(identity, accumulator, combiner);
    }

    public <R> R collect(Supplier<R> supplier, BiConsumer<R, ? super T> accumulator, BiConsumer<R, R> combiner) {
        return stream.collect(supplier, accumulator, combiner);
    }

    public <R, A> R collect(Collector<? super T, A, R> collector) {
        return stream.collect(collector);
    }

    public Optional<T> min(Comparator<? super T> comparator) {
        return stream.min(comparator);
    }

    public Optional<T> max(Comparator<? super T> comparator) {
        return stream.max(comparator);
    }

    public long count() {
        return stream.count();
    }

    public boolean anyMatch(Predicate<? super T> predicate) {
        return stream.anyMatch(predicate);
    }

    public boolean allMatch(Predicate<? super T> predicate) {
        return stream.allMatch(predicate);
    }

    public boolean noneMatch(Predicate<? super T> predicate) {
        return stream.noneMatch(predicate);
    }

    public Optional<T> findFirst() {
        return stream.findFirst();
    }

    public Optional<T> findAny() {
        return stream.findAny();
    }

    public static <T> Stream.Builder<T> builder() {
        return Stream.builder();
    }

    public static <T> Stream<T> empty() {
        return Stream.empty();
    }

    public static <T> Stream<T> of(T t) {
        return Stream.of(t);
    }

    public static <T> Stream<T> of(T... values) {
        return Stream.of(values);
    }

    public static <T> Stream<T> iterate(T seed, UnaryOperator<T> f) {
        return Stream.iterate(seed, f);
    }

    public static <T> Stream<T> generate(Supplier<T> s) {
        return Stream.generate(s);
    }

    public static <T> Stream<T> concat(Stream<? extends T> a, Stream<? extends T> b) {
        return Stream.concat(a, b);
    }

    public Iterator<T> iterator() {
        return stream.iterator();
    }

    public Spliterator<T> spliterator() {
        return stream.spliterator();
    }

    public boolean isParallel() {
        return stream.isParallel();
    }

    public Stream<T> sequential() {
        return stream.sequential();
    }

    public Stream<T> parallel() {
        return stream.parallel();
    }

    public Stream<T> unordered() {
        return stream.unordered();
    }

    public Stream<T> onClose(Runnable closeHandler) {
        return stream.onClose(closeHandler);
    }

    public void close() {
        stream.close();
    }

    // EXCEPTION REFERENCE
    public ThrowablePocket<SQLException> getException() {
        return exception_pocket;
    }

}
