/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.sql;

import com.smmx.maria.commons.database.connections.ConnectionShield;
import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;
import java.util.Map;

/**
 * @author Osvaldo Miguel Colin
 */
public class PreparedStatementFactory {

    private final String sql;
    private final Map<String, Class<?>> fields;
    private final PreparedParameters parameters;

    public PreparedStatementFactory(String sql, Map<String, Class<?>> fields, PreparedParameters parameters) {
        // INIT
        this.sql = StringUtils.trimToEmpty(sql);
        this.fields = fields;
        this.parameters = parameters;
    }

    // PREPARED STATEMENT
    public PreparedStatementShield preparedStatement(ConnectionShield connection) throws SQLException {
        return new PreparedStatementShield(connection.prepareStatement(sql), fields, parameters);
    }
}
