/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.serialization.primitives;

import com.smmx.maria.commons.database.serialization.SQLSerializer;

/**
 * @author Osvaldo Miguel Colin
 */
public class FloatSQLSerializer extends SQLSerializer<Float, Float> {

    public FloatSQLSerializer() {
        super(Float.class, Float.class);
    }

    @Override
    public Float outgoing(Float value) {
        return value;
    }

}
