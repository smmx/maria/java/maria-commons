/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.resultsets;

import com.smmx.maria.commons.database.sql.PreparedFields;
import com.smmx.maria.commons.pockets.ThrowablePocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

/**
 * @author Osvaldo Miguel Colin
 */
public class ResultSetShield {

    // STATIC
    private static final Logger LOGGER = LoggerFactory.getLogger(ResultSetShield.class);

    public static ResultSetStream<List> getEmptyRowStream() throws SQLException {
        ThrowablePocket<SQLException> exception_pocket = new ThrowablePocket<>();
        return new ResultSetStream(Stream.empty(), exception_pocket);
    }

    public static ResultSetStream<Map<String, Object>> getEmptyMapStream() throws SQLException {
        ThrowablePocket<SQLException> exception_pocket = new ThrowablePocket<>();
        return new ResultSetStream(Stream.empty(), exception_pocket);
    }

    // MEMBER
    private boolean closed;

    private final Statement statement;
    private final ResultSet result_set;
    private final PreparedFields fields;

    public ResultSetShield(Statement statement, ResultSet result_set, Map<String, Class<?>> fields) throws SQLException {
        this.closed = false;
        this.statement = statement;
        this.result_set = result_set;

        // GET FIELDS
        String[] fieldnames = ResultSetUtils.getFieldNames(result_set);
        this.fields = new PreparedFields(fields, fieldnames);
    }

    public void close() {
        if (!closed) {
            closed = true;

            // CLOSE RESULT SET
            try {
                result_set.close();
            } catch (SQLException ex) {
                LOGGER.error("Resulset close failed.", ex);
            }

            // CLOSE STATEMENT
            try {
                statement.close();
            } catch (SQLException ex) {
                LOGGER.error("Statement close failed.", ex);
            }
        }
    }

    public ResultSetStream<List> getRowStream() throws SQLException {
        ThrowablePocket<SQLException> exception_pocket = new ThrowablePocket<>();
        Stream<List> stream = getRowStream(exception_pocket);
        return new ResultSetStream(stream, exception_pocket);
    }

    private Stream<List> getRowStream(ThrowablePocket<SQLException> exception_pocket) throws SQLException {
        // CREATE ITERATOR
        Iterable<List> iterable = () -> new ResultSetIterator(result_set, fields.getDatatypes(), this::close, exception_pocket);

        // CREATE STREAM
        return StreamSupport.stream(iterable.spliterator(), false)
            .filter(row -> row != null)
            .onClose(this::close);
    }

    public ResultSetStream<Map<String, Object>> getMapStream() throws SQLException {
        ThrowablePocket<SQLException> exception_pocket = new ThrowablePocket<>();

        Stream<Map<String, Object>> stream = getRowStream(exception_pocket)
            .map((List row) -> {
                Map<String, Object> map = new LinkedHashMap<>();

                for (int i = 0; i < fields.size(); i++) {
                    map.put(fields.getNames()[i], row.get(i));
                }

                return Collections.unmodifiableMap(map);
            });

        return new ResultSetStream(stream, exception_pocket);
    }

}
