/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.sql;

/**
 * @author Osvaldo Miguel Colin
 */
public class PreparedStatementException extends RuntimeException {

    public PreparedStatementException(String msg) {
        super(msg);
    }

}
