/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.sql;

import com.smmx.maria.commons.database.serialization.SQLSerializationManager;
import com.smmx.maria.commons.database.resultsets.ResultSetShield;
import com.smmx.maria.commons.database.resultsets.ResultSetStream;

import java.math.BigDecimal;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.*;

import static com.smmx.maria.commons.MariaCommons.sf;

/**
 * @author Osvaldo Miguel Colin
 */
public class PreparedStatementShield {

    private final PreparedStatement prepared_statement;
    private final Map<String, Class<?>> fields;
    private final PreparedParameters parameters;

    public PreparedStatementShield(PreparedStatement prepared_statement, Map<String, Class<?>> fields, PreparedParameters parameters) {
        this.prepared_statement = prepared_statement;
        this.fields = fields;
        this.parameters = parameters;
    }

    // GETTERS
    public PreparedStatement getPreparedStatement() {
        return prepared_statement;
    }

    // MAPPING
    public void setStatementParameters(Map<String, Object> arguments) throws SQLException {
        // MAP TO ARRAY
        Object[] arg_array = new Object[parameters.size()];

        for (int i = 0; i < parameters.size(); i++) {
            String parameter = parameters.getNames()[i];

            if (!arguments.containsKey(parameter)) {
                throw new PreparedStatementException(sf(
                    "Missing argument for parameter \"%s\". \nArgs: %s",
                    parameter, arguments.toString()
                ));
            }

            arg_array[i] = arguments.get(parameter);
        }

        // SET PARAMETERS
        for (int index = 0; index < parameters.size(); index++) {
            int index_ofs_1 = index + 1;

            // GET ARGS AND PARAMETER DT
            Object argument = arg_array[index];
            Class<?> datatype = parameters.getDatatypes()[index];

            // SQLVALUE
            Object value = SQLSerializationManager.getInstance().outgoing(argument, datatype);
            Class<?> clazz = SQLSerializationManager.getInstance().handler(datatype).getOutgoingClass();

            // SET PARAMETER
            if (clazz.equals(byte[].class)) {
                if (value != null) {
                    prepared_statement.setBytes(index_ofs_1, (byte[]) value);
                } else {
                    prepared_statement.setNull(index_ofs_1, java.sql.Types.BINARY);
                }
            } else if (clazz.equals(String.class)) {
                if (value != null) {
                    prepared_statement.setString(index_ofs_1, (String) value);
                } else {
                    prepared_statement.setNull(index_ofs_1, java.sql.Types.VARCHAR);
                }
            } else if (clazz.equals(Integer.class)) {
                if (value != null) {
                    prepared_statement.setInt(index_ofs_1, (Integer) value);
                } else {
                    prepared_statement.setNull(index_ofs_1, java.sql.Types.INTEGER);
                }
            } else if (clazz.equals(Float.class)) {
                if (value != null) {
                    prepared_statement.setFloat(index_ofs_1, (Float) value);
                } else {
                    prepared_statement.setNull(index_ofs_1, java.sql.Types.FLOAT);
                }
            } else if (clazz.equals(Double.class)) {
                if (value != null) {
                    prepared_statement.setDouble(index_ofs_1, (Double) value);
                } else {
                    prepared_statement.setNull(index_ofs_1, java.sql.Types.DOUBLE);
                }
            } else if (clazz.equals(Long.class)) {
                if (value != null) {
                    prepared_statement.setLong(index_ofs_1, (Long) value);
                } else {
                    prepared_statement.setNull(index_ofs_1, java.sql.Types.BIGINT);
                }
            } else if (clazz.equals(BigDecimal.class)) {
                if (value != null) {
                    prepared_statement.setBigDecimal(index_ofs_1, (BigDecimal) value);
                } else {
                    prepared_statement.setNull(index_ofs_1, java.sql.Types.DECIMAL);
                }
            } else if (clazz.equals(Boolean.class)) {
                if (value != null) {
                    prepared_statement.setBoolean(index_ofs_1, (Boolean) value);
                } else {
                    prepared_statement.setNull(index_ofs_1, java.sql.Types.BOOLEAN);
                }
            } else if (clazz.equals(java.sql.Time.class)) {
                if (value != null) {
                    prepared_statement.setTime(index_ofs_1, (java.sql.Time) value);
                } else {
                    prepared_statement.setNull(index_ofs_1, java.sql.Types.TIME);
                }
            } else if (clazz.equals(java.sql.Date.class)) {
                if (value != null) {
                    prepared_statement.setDate(index_ofs_1, (java.sql.Date) value);
                } else {
                    prepared_statement.setNull(index_ofs_1, java.sql.Types.DATE);
                }
            } else if (clazz.equals(java.sql.Timestamp.class)) {
                if (value != null) {
                    prepared_statement.setTimestamp(index_ofs_1, (java.sql.Timestamp) value);
                } else {
                    prepared_statement.setNull(index_ofs_1, java.sql.Types.TIMESTAMP);
                }
            } else if (clazz.equals(java.sql.Blob.class)) {
                if (value != null) {
                    prepared_statement.setBlob(index_ofs_1, (java.sql.Blob) value);
                } else {
                    prepared_statement.setNull(index_ofs_1, Types.BLOB);
                }
            } else if (clazz.equals(java.sql.Clob.class)) {
                if (value != null) {
                    prepared_statement.setClob(index_ofs_1, (java.sql.Clob) value);
                } else {
                    prepared_statement.setNull(index_ofs_1, Types.CLOB);
                }
            } else if (clazz.equals(java.sql.NClob.class)) {
                if (value != null) {
                    prepared_statement.setNClob(index_ofs_1, (java.sql.NClob) value);
                } else {
                    prepared_statement.setNull(index_ofs_1, Types.NCLOB);
                }
            } else {
                if (value != null) {
                    prepared_statement.setObject(index_ofs_1, value);
                } else {
                    prepared_statement.setNull(index_ofs_1, java.sql.Types.JAVA_OBJECT);
                }
            }
        }
    }

    // PRIMITIVE
    public ResultSetShield query() throws SQLException {
        return query(Collections.emptyMap());
    }

    public ResultSetShield query(Map<String, Object> arguments) throws SQLException {
        // SET PARAMETERS
        setStatementParameters(arguments);

        // QUERY
        ResultSet result_set = prepared_statement.executeQuery();

        // SHIELD RESULT SET
        return new ResultSetShield(prepared_statement, result_set, fields);
    }

    public int update() throws SQLException {
        return update(Collections.emptyMap());
    }

    public int update(Map<String, Object> arguments) throws SQLException {
        // SET PARAMETERS
        setStatementParameters(arguments);

        // UPDATE
        int res;

        try {
            res = prepared_statement.executeUpdate();
        } finally {
            prepared_statement.close();
        }

        // RETURN
        return res;
    }

    public boolean execute() throws SQLException {
        return execute(Collections.emptyMap());
    }

    public boolean execute(Map<String, Object> arguments) throws SQLException {
        // SET PARAMETERS
        setStatementParameters(arguments);

        // EXECUTE
        boolean res;

        try {
            res = prepared_statement.execute();
        } finally {
            prepared_statement.close();
        }

        return res;
    }

    public int[] executeBatch() throws SQLException {
        int[] res;

        try {
            res = prepared_statement.executeBatch();
        } finally {
            prepared_statement.close();
        }

        return res;
    }

    public long[] executeLargeBatch() throws SQLException {
        long[] res;

        try {
            res = prepared_statement.executeLargeBatch();
        } finally {
            prepared_statement.close();
        }

        return res;
    }

    // BATCH
    public void addBatch(Map<String, Object> arguments) throws SQLException {
        // SET PARAMETERS
        setStatementParameters(arguments);

        // ADD
        prepared_statement.addBatch();
    }

    // QUERIES
    public Optional<Object> queryAndGetFirstValue() throws SQLException {
        return queryAndGetFirstValue(Collections.emptyMap());
    }

    public Optional<Object> queryAndGetFirstValue(Map<String, Object> args) throws SQLException {
        List row = queryAndGetFirstRow(args).orElse(null);

        if (row != null) {
            if (row.size() > 0) {
                return Optional.ofNullable(row.get(0));
            }
        }

        return Optional.empty();
    }

    public Optional<Map<String, Object>> queryAndGetFirstMap() throws SQLException {
        return queryAndGetFirstMap(Collections.emptyMap());
    }

    public Optional<Map<String, Object>> queryAndGetFirstMap(Map<String, Object> args) throws SQLException {
        ResultSetStream<Map<String, Object>> rs_stream = queryAndStreamMaps(args);
        Optional<Map<String, Object>> first = rs_stream.findFirst();
        rs_stream.getException().throwIfPresent();
        rs_stream.close();
        return first;
    }

    public List<Map<String, Object>> queryAndGetMaps() throws SQLException {
        return queryAndGetMaps(Collections.emptyMap());
    }

    public List<Map<String, Object>> queryAndGetMaps(Map<String, Object> args) throws SQLException {
        List<Map<String, Object>> list = new ArrayList<>();

        ResultSetStream<Map<String, Object>> rs_stream = queryAndStreamMaps(args);
        rs_stream.forEach(list::add);
        rs_stream.getException().throwIfPresent();

        return Collections.unmodifiableList(list);
    }

    public ResultSetStream<Map<String, Object>> queryAndStreamMaps() throws SQLException {
        return queryAndStreamMaps(Collections.emptyMap());
    }

    public ResultSetStream<Map<String, Object>> queryAndStreamMaps(Map<String, Object> args) throws SQLException {
        ResultSetShield result_set = query(args);

        if (result_set != null) {
            return result_set.getMapStream();
        }

        return ResultSetShield.getEmptyMapStream();
    }

    public Optional<List> queryAndGetFirstRow() throws SQLException {
        return queryAndGetFirstRow(Collections.emptyMap());
    }

    public Optional<List> queryAndGetFirstRow(Map<String, Object> args) throws SQLException {
        ResultSetStream<List> rs_stream = queryAndStreamRows(args);
        Optional<List> first = rs_stream.findFirst();
        rs_stream.getException().throwIfPresent();
        rs_stream.close();
        return first;
    }

    public List<List> queryAndGetRows() throws SQLException {
        return queryAndGetRows(Collections.emptyMap());
    }

    public List<List> queryAndGetRows(Map<String, Object> args) throws SQLException {
        List<List> list = new ArrayList<>();

        ResultSetStream<List> rs_stream = queryAndStreamRows(args);
        rs_stream.forEach(list::add);
        rs_stream.getException().throwIfPresent();
        rs_stream.close();

        return Collections.unmodifiableList(list);
    }

    public ResultSetStream<List> queryAndStreamRows() throws SQLException {
        return queryAndStreamRows(Collections.emptyMap());
    }

    public ResultSetStream<List> queryAndStreamRows(Map<String, Object> args) throws SQLException {
        ResultSetShield result_set = query(args);

        if (result_set != null) {
            return result_set.getRowStream();
        }

        return ResultSetShield.getEmptyRowStream();
    }

}
