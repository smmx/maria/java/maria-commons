/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.serialization.primitives;

import com.smmx.maria.commons.database.serialization.SQLSerializer;

import java.sql.NClob;

/**
 * @author Osvaldo Miguel Colin
 */
public class SQLNClobSQLSerializer extends SQLSerializer<NClob, NClob> {

    public SQLNClobSQLSerializer() {
        super(NClob.class, NClob.class);
    }

    @Override
    public NClob outgoing(NClob value) {
        return value;
    }

}
