/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.sql;

import java.util.Arrays;
import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.sf;

/**
 * @author Osvaldo Miguel Colin
 */
public class PreparedParameters {

    private final String[] names;
    private final Class<?>[] datatypes;

    public PreparedParameters(String[] ordered_parameters, Map<String, Class<?>> parameters) {
        // VALIDATE PARAMETERS
        for (String parameter : parameters.keySet()) {
            boolean doesnt_have = Arrays.stream(ordered_parameters)
                .noneMatch(name -> {
                    return parameter.equals(name);
                });

            if (doesnt_have) {
                throw new PreparedStatementException(sf(
                    "Parameter \"%s\" datatype provided, but not used in statement.",
                    parameter
                ));
            }
        }

        for (String parameter : ordered_parameters) {
            if (!parameters.containsKey(parameter)) {
                throw new PreparedStatementException(sf(
                    "Parameter \"%s\" used in statement, but datatype wasn't provided.",
                    parameter
                ));
            }
        }

        // EXTRACT
        names = new String[ordered_parameters.length];
        datatypes = new Class<?>[ordered_parameters.length];

        for (int i = 0; i < ordered_parameters.length; i++) {
            String name = ordered_parameters[i];
            Class<?> datatype = parameters.get(name);

            names[i] = name;
            datatypes[i] = datatype;
        }
    }

    public String[] getNames() {
        return names;
    }

    public Class<?>[] getDatatypes() {
        return datatypes;
    }

    public int size() {
        return names.length;
    }

}
