/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.connections;

import java.sql.SQLException;

/**
 * @author Osvaldo Miguel Colin
 */
public interface ConnectionOperation {

    void run(ConnectionShield connection) throws SQLException;

}
