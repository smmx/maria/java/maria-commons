/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.sql;

import java.util.Arrays;
import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.sf;

/**
 * @author Osvaldo Miguel Colin
 */
public class PreparedFields {

    private final String[] names;
    private final Class<?>[] datatypes;

    public PreparedFields(Map<String, Class<?>> fields, String[] order) {
        // VALIDATE PARAMETERS
        for (String field : fields.keySet()) {
            boolean doesnt_contain = Arrays.stream(order)
                .noneMatch(fieldname -> fieldname.equals(field));

            if (doesnt_contain) {
                throw new PreparedStatementException(sf(
                    "Field \"%s\" datatype provided, but not used in statement.",
                    field
                ));
            }
        }

        for (String field : order) {
            if (!fields.containsKey(field)) {
                throw new PreparedStatementException(sf(
                    "Field \"%s\" used in statement, but datatype wasn't provided.",
                    field
                ));
            }
        }

        // EXTRACT
        names = new String[order.length];
        datatypes = new Class<?>[order.length];

        for (int i = 0; i < order.length; i++) {
            String name = order[i];
            Class<?> datatype = fields.get(name);

            names[i] = name;
            datatypes[i] = datatype;
        }
    }

    public String[] getNames() {
        return names;
    }

    public Class<?>[] getDatatypes() {
        return datatypes;
    }

    public int size() {
        return names.length;
    }

}
