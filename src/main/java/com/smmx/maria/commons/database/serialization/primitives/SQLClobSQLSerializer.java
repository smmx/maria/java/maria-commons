/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.serialization.primitives;

import com.smmx.maria.commons.database.serialization.SQLSerializer;

import java.sql.Clob;

/**
 * @author Osvaldo Miguel Colin
 */
public class SQLClobSQLSerializer extends SQLSerializer<Clob, Clob> {

    public SQLClobSQLSerializer() {
        super(Clob.class, Clob.class);
    }

    @Override
    public Clob outgoing(Clob value) {
        return value;
    }

}
