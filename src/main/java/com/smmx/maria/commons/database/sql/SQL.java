/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.sql;

import com.smmx.maria.commons.database.connections.ConnectionShield;
import org.apache.commons.lang3.StringUtils;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Osvaldo Miguel Colin
 */
public class SQL {

    private final String sql;
    private final Map<String, Class<?>> fields;
    private final Map<String, Class<?>> parameters;

    public SQL(String sql) {
        this.sql = StringUtils.trimToEmpty(sql);
        this.fields = new HashMap<>();
        this.parameters = new HashMap<>();
    }

    // GETTERS
    public String getSQL() {
        return sql;
    }

    public Map<String, Class<?>> getFields() {
        return fields;
    }

    public Map<String, Class<?>> getParameters() {
        return parameters;
    }

    // FUNCTIONAL
    public SQL registerField(String name, Class<?> datatype) {
        this.fields.put(name, datatype);
        return this;
    }

    public SQL registerParameter(String name, Class<?> datatype) {
        this.parameters.put(name, datatype);
        return this;
    }

    public SQL registerFields(Map<String, Class<?>> fields) {
        this.fields.putAll(fields);
        return this;
    }

    public SQL registerParameters(Map<String, Class<?>> parameters) {
        this.parameters.putAll(parameters);
        return this;
    }

    // STATEMENT
    private static final Pattern PARAMETER_REGEX = Pattern.compile(":(\\w+)|('[^\\']*')|(\"[^\\\"]*\")");

    public PreparedStatementFactory build() {
        // INIT
        List<String> parameters_found = new ArrayList<>();
        StringBuffer prepared_sql_sb = new StringBuffer();

        // MATCH
        Matcher parameter_matcher = PARAMETER_REGEX.matcher(sql);

        while (parameter_matcher.find()) {
            String match = parameter_matcher.group();
            String parameter = parameter_matcher.group(1);
            String quoted_text_sq = parameter_matcher.group(2);
            String quoted_text_dq = parameter_matcher.group(3);

            if (parameter != null) {
                // EXTRACT PARAMETERS
                parameters_found.add(parameter);

                // REPLACE PARAMETERS WITH PREPARED STATEMENT PARAMETER (?)
                parameter_matcher.appendReplacement(prepared_sql_sb, match.replace(":" + parameter, "?"));
            } else if (quoted_text_sq != null) {
                parameter_matcher.appendReplacement(prepared_sql_sb, quoted_text_sq);
            } else if (quoted_text_dq != null) {
                parameter_matcher.appendReplacement(prepared_sql_sb, quoted_text_dq);
            }
        }

        parameter_matcher.appendTail(prepared_sql_sb);

        // PREPARE
        String prepared_sql = prepared_sql_sb.toString();
        String[] parameter_names = parameters_found.toArray(new String[parameters_found.size()]);
        PreparedParameters prepared_parameters = new PreparedParameters(parameter_names, parameters);

        // RETURN
        return new PreparedStatementFactory(prepared_sql, fields, prepared_parameters);
    }

    public PreparedStatementShield preparedStatement(ConnectionShield connection) throws SQLException {
        return build().preparedStatement(connection);
    }

}
