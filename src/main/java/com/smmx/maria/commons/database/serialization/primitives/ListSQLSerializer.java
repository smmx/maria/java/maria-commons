/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.serialization.primitives;

import com.smmx.maria.commons.database.serialization.SQLSerializer;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Osvaldo Miguel Colin
 */
public class ListSQLSerializer extends SQLSerializer<List, String> {

    public ListSQLSerializer() {
        super(List.class, String.class);
    }

    @Override
    public String outgoing(List value) {
        return value.stream().collect(Collectors.joining(";")).toString();
    }

}
