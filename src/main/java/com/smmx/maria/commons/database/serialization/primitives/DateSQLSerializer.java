
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.serialization.primitives;

import com.smmx.maria.commons.database.serialization.SQLSerializer;

import java.sql.Timestamp;
import java.util.Date;

/**
 * @author Osvaldo Miguel Colin
 */
public class DateSQLSerializer extends SQLSerializer<Date, Timestamp> {

    public DateSQLSerializer() {
        super(Date.class, java.sql.Timestamp.class);
    }

    @Override
    public java.sql.Timestamp outgoing(Date value) {
        return new java.sql.Timestamp(value.getTime());
    }

}
