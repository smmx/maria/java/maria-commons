/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.sql;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Osvaldo Miguel Colin
 */
public class SQLContext {

    private final Map<String, SQL> definitions;

    public SQLContext() {
        definitions = new HashMap<>();
    }

    public SQLTemplate sql(String sql) {
        SQLTemplate template = new SQLTemplate(sql);
        template.define(definitions);
        return template;
    }

    public void define(String name, SQL snippet) {
        definitions.put(name, snippet);
    }

    public void define(Map<String, SQL> snippets) {
        this.definitions.putAll(snippets);
    }

    public boolean has(String name) {
        return definitions.containsKey(name);
    }

    public SQL get(String name) {
        return definitions.get(name);
    }

    public Map<String, SQL> getDefinitions() {
        return definitions;
    }

}
