/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.database.serialization.primitives;

import com.smmx.maria.commons.database.serialization.SQLSerializer;
import org.json.JSONObject;

import java.util.Map;

/**
 * @author Osvaldo Miguel Colin
 */
public class MapSQLSerializer extends SQLSerializer<Map, String> {

    public MapSQLSerializer() {
        super(Map.class, String.class);
    }

    @Override
    public String outgoing(Map value) {
        return new JSONObject(value).toString();
    }

}
