/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.values.processors;

import com.smmx.maria.commons.optional.OptionalValue;
import com.smmx.maria.commons.values.errors.ValueError;
import com.smmx.maria.commons.values.interpreters.InterpreterException;
import com.smmx.maria.commons.values.interpreters.InterpreterManager;

import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static com.smmx.maria.commons.MariaCommons.sf;

/**
 * @param <INPUT>
 * @author Osvaldo Miguel Colin
 */
public class DictionaryProcessor<INPUT> implements Function<INPUT, Map<String, Object>> {

    private final String blame;
    private final Function<INPUT, Map<String, Object>> parent;

    public DictionaryProcessor(Function<INPUT, Map<String, Object>> parent) {
        this(null, parent);
    }

    public DictionaryProcessor(String blame, Function<INPUT, Map<String, Object>> parent) {
        // PARENT
        this.parent = parent;

        // BLAME
        this.blame = blame;
    }

    // DEF
    public DictionaryProcessor<INPUT> check(Predicate<Map<String, Object>> predicate, String err_msg) {
        return new DictionaryProcessor(
            blame,
            (input) -> {
                Map<String, Object> value = parent.apply((INPUT) input);

                if (!predicate.test(value)) {
                    throw new ValueError(err_msg);
                }

                return value;
            }
        );
    }

    public DictionaryProcessor<INPUT> when(Predicate<Map<String, Object>> predicate, Map<String, Object> then) {
        return new DictionaryProcessor(
            blame,
            (input) -> {
                Map<String, Object> value = parent.apply((INPUT) input);

                if (predicate.test(value)) {
                    return then;
                }

                return value;
            }
        );
    }

    public DictionaryProcessor<INPUT> when(Predicate<Map<String, Object>> predicate, Map<String, Object> then, Map<String, Object> or_else) {
        return new DictionaryProcessor(
            blame,
            (input) -> {
                Map<String, Object> value = parent.apply((INPUT) input);

                if (predicate.test(value)) {
                    return then;
                }

                return or_else;
            }
        );
    }

    public <O> ValueProcessor<INPUT, O> map(Function<Map<String, Object>, O> mapper) {
        return new ValueProcessor(
            blame,
            (input) -> {
                Map<String, Object> value = parent.apply((INPUT) input);

                // RETURN
                return mapper.apply(value);
            }
        );
    }

    public DictionaryProcessor<INPUT> peek(Consumer<Map<String, Object>> consumer) {
        return new DictionaryProcessor(
            blame,
            (input) -> {
                Map<String, Object> value = parent.apply((INPUT) input);

                // PEEK
                consumer.accept(value);

                // RETURN
                return value;
            }
        );
    }

    // CHECKS
    public DictionaryProcessor<INPUT> notNull() {
        return check(Objects::nonNull, "Input can't be null.");
    }

    public DictionaryProcessor<INPUT> ifNull(Map<String, Object> then) {
        return when(Objects::isNull, then);
    }

    // CAST
    public <T> ValueProcessor<INPUT, T> anyCast(Class<T> clazz) {
        return map(value -> {
            try {
                return InterpreterManager.getInstance()
                    .valueOf(value, clazz);
            } catch (InterpreterException ex) {
                throw new ValueError(sf("Map can't be converted to %s: %s", clazz.getName(), ex.getMessage()));
            }
        });
    }

    public <T> ValueProcessor<INPUT, T> staticCast(Class<T> clazz) {
        return map(value -> {
            // CHECKS
            if (value == null) {
                return null;
            }

            // CAST
            try {
                return (T) value;
            } catch (ClassCastException ex) {
                throw new ValueError(sf("Value can't be cast to type %s: %s", clazz.getName(), ex.getMessage()));
            }
        });
    }

    // CUSTOM
    public ValueProcessor<INPUT, Object> get(String key) {
        return DictionaryProcessor.this.get(key, null, false);
    }

    public ValueProcessor<INPUT, Object> get(String key, Object def) {
        return DictionaryProcessor.this.get(key, def, false);
    }

    public ValueProcessor<INPUT, Object> get(String key, Object def, boolean null_to_default) {
        return new ValueProcessor(
            (blame != null ? blame + "->" : "") + key,
            (input) -> {
                Map<String, Object> map = parent.apply((INPUT) input);

                if (null_to_default) {
                    // GET RETURNS NULL IF IT'S NOT PRESENT
                    // AND ALSO RETURNS NULL IF THE VALUE ASIGNED IS NULL
                    Object value = map.get(key);

                    if (value == null) {
                        return def;
                    }

                    return value;
                } else {
                    // THIS ALLOWS NULL TO BE RETURNED IF IT'S PRESENT
                    // IN THE MAP
                    return map.getOrDefault(key, def);
                }
            }
        );
    }

    public ValueProcessor<INPUT, Object> require(String key) {
        return require(
            key,
            "value wasn't given."
        );
    }

    public ValueProcessor<INPUT, Object> require(String key, String err_msg) {
        return new ValueProcessor(
            (blame != null ? blame + "->" : "") + key,
            (input) -> {
                Map<String, Object> map = parent.apply((INPUT) input);

                if (!map.containsKey(key)) {
                    throw new ValueError(err_msg);
                }

                // RETURN
                return map.get(key);
            }
        );
    }

    public <O> OptionalProcessor<INPUT, O> optional(String key, Function<Object, O> processor) {
        return new OptionalProcessor(
            (blame != null ? blame + "->" : "") + key,
            (input) -> {
                Map<String, Object> map = parent.apply((INPUT) input);

                // EVAL
                if (map.containsKey(key)) {
                    Object val = map.get(key);
                    val = processor.apply(val);
                    return OptionalValue.of(val);
                }

                // RETURN
                return OptionalValue.undefined();
            }
        );
    }

    // IMPLEMENT
    @Override
    public Map<String, Object> apply(INPUT input) {
        try {
            return parent.apply(input);
        } catch (Throwable ex) {
            if (this.blame == null) {
                throw new ValueError(ex.getMessage());
            } else {
                throw new ValueError(this.blame + "->" + ex.getMessage());
            }
        }
    }

}
