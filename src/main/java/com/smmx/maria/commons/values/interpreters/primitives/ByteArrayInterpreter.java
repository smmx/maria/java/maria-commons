/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.values.interpreters.primitives;

import com.smmx.maria.commons.values.interpreters.Interpreter;
import com.smmx.maria.commons.values.interpreters.InterpreterException;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;

import java.util.Objects;

import static com.smmx.maria.commons.MariaCommons.sf;

/**
 * @author Osvaldo Miguel Colin
 */
public class ByteArrayInterpreter extends Interpreter<byte[]> {

    public ByteArrayInterpreter() {
        super(byte[].class);
    }

    @Override
    public byte[] apply(Object value) {
        // NULL CHECK
        if (value == null) {
            return null;
        }

        // TYPE CHECK
        if (value instanceof byte[]) {
            return (byte[]) value;
        }

        // VALUEOF
        if (value instanceof String) {
            String bin = (String) value;

            try {
                return Hex.decodeHex(bin.toCharArray());
            } catch (DecoderException ex) {
                // NOT A BINARY
            }
        }

        throw new InterpreterException(sf("\"%s\" can't be converted to a byte array", Objects.toString(value)));
    }

}
