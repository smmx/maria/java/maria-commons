/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.values.interpreters.primitives;

import com.smmx.maria.commons.values.interpreters.Interpreter;
import com.smmx.maria.commons.values.interpreters.InterpreterException;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

import static com.smmx.maria.commons.MariaCommons.sf;

/**
 * @author Osvaldo Miguel Colin
 */
public class ListInterpreter extends Interpreter<List> {

    public ListInterpreter() {
        super(List.class);
    }

    @Override
    public List apply(Object value) {
        // NULL CHECK
        if (value == null) {
            return null;
        }

        // TYPE CHECK
        if (value instanceof List) {
            return (List) value;
        }

        // VALUEOF
        if (value instanceof String) {
            String str_value = StringUtils.trimToNull((String) value);

            if (str_value == null) {
                return null;
            }

            String[] parts = str_value.split(";");
            return Arrays.asList(parts);
        }

        throw new InterpreterException(sf("\"%s\" can't be converted to list.", Objects.toString(value)));
    }

}
