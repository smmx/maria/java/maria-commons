/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.values.processors;

import com.smmx.maria.commons.optional.OptionalValue;
import com.smmx.maria.commons.values.errors.ValueError;
import com.smmx.maria.commons.values.interpreters.InterpreterException;
import com.smmx.maria.commons.values.interpreters.InterpreterManager;

import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static com.smmx.maria.commons.MariaCommons.sf;

/**
 * @param <INPUT>
 * @author Osvaldo Miguel Colin
 */
public class ListProcessor<INPUT, ITEM> implements Function<INPUT, List<ITEM>> {

    private final String blame;
    private final Function<INPUT, List<ITEM>> parent;

    public ListProcessor(Function<INPUT, List<ITEM>> parent) {
        this(null, parent);
    }

    public ListProcessor(String blame, Function<INPUT, List<ITEM>> parent) {
        // PARENT
        this.parent = parent;

        // BLAME
        this.blame = blame;
    }

    // DEF
    public ListProcessor<INPUT, ITEM> check(Predicate<List<ITEM>> predicate, String err_msg) {
        return new ListProcessor(
            blame,
            (input) -> {
                List<ITEM> value = parent.apply((INPUT) input);

                if (!predicate.test(value)) {
                    throw new ValueError(err_msg);
                }

                return value;
            }
        );
    }

    public ListProcessor<INPUT, ITEM> when(Predicate<List<ITEM>> predicate, List<ITEM> then) {
        return new ListProcessor(
            blame,
            (input) -> {
                List<ITEM> value = parent.apply((INPUT) input);

                if (predicate.test(value)) {
                    return then;
                }

                return value;
            }
        );
    }

    public ListProcessor<INPUT, ITEM> when(Predicate<List<ITEM>> predicate, List<ITEM> then, List<ITEM> or_else) {
        return new ListProcessor(
            blame,
            (input) -> {
                List<ITEM> value = parent.apply((INPUT) input);

                if (predicate.test(value)) {
                    return then;
                }

                return or_else;
            }
        );
    }

    public <O> ValueProcessor<INPUT, O> map(Function<List<ITEM>, O> mapper) {
        return new ValueProcessor(
            blame,
            (input) -> {
                List<ITEM> value = parent.apply((INPUT) input);

                // RETURN
                return mapper.apply(value);
            }
        );
    }

    public ListProcessor<INPUT, ITEM> peek(Consumer<List<ITEM>> consumer) {
        return new ListProcessor(
            blame,
            (input) -> {
                List<ITEM> value = parent.apply((INPUT) input);

                // PEEK
                consumer.accept(value);

                // RETURN
                return value;
            }
        );
    }

    // CHECKS
    public ListProcessor<INPUT, ITEM> notNull() {
        return check(Objects::nonNull, "Input can't be null.");
    }

    public ListProcessor<INPUT, ITEM> ifNull(List<ITEM> then) {
        return when(Objects::isNull, then);
    }

    // CAST
    public <T> ValueProcessor<INPUT, T> anyCast(Class<T> clazz) {
        return map(value -> {
            try {
                return InterpreterManager.getInstance()
                    .valueOf(value, clazz);
            } catch (InterpreterException ex) {
                throw new ValueError(sf("List can't be converted to %s: %s", clazz.getName(), ex.getMessage()));
            }
        });
    }

    public <T> ValueProcessor<INPUT, T> staticCast(Class<T> clazz) {
        return map(value -> {
            // CHECKS
            if (value == null) {
                return null;
            }

            // CAST
            try {
                return (T) value;
            } catch (ClassCastException ex) {
                throw new ValueError(sf("Value can't be cast to type %s: %s", clazz.getName(), ex.getMessage()));
            }
        });
    }

    // CUSTOM
    public ValueProcessor<INPUT, ITEM> get(int index) {
        return this.get(index, null);
    }

    public ValueProcessor<INPUT, ITEM> get(int index, Object def) {
        return new ValueProcessor(
            (blame != null ? blame + "->" : "") + index,
            (input) -> {
                List<ITEM> list = parent.apply((INPUT) input);

                if (index < list.size()) {
                    return list.get(index);
                }

                return def;
            }
        );
    }

    public ValueProcessor<INPUT, ITEM> require(int index) {
        return require(
            index,
            "Index out of bounds."
        );
    }

    public ValueProcessor<INPUT, ITEM> require(int index, String err_msg) {
        return new ValueProcessor(
            (blame != null ? blame + "->" : "") + index,
            (input) -> {
                List<ITEM> list = parent.apply((INPUT) input);

                if (index < list.size()) {
                    return list.get(index);
                }

                throw new ValueError(err_msg);
            }
        );
    }

    public <O> OptionalProcessor<INPUT, O> optional(int index, Function<ITEM, O> processor) {
        return new OptionalProcessor(
            (blame != null ? blame + "->" : "") + index,
            (input) -> {
                List<ITEM> list = parent.apply((INPUT) input);

                if (index < list.size()) {
                    ITEM val = list.get(index);

                    return OptionalValue.of(
                        processor.apply(val)
                    );
                }

                return OptionalValue.undefined();
            }
        );
    }

    // IMPLEMENT
    @Override
    public List<ITEM> apply(INPUT input) {
        try {
            return parent.apply(input);
        } catch (Throwable ex) {
            if (this.blame == null) {
                throw new ValueError(ex.getMessage());
            } else {
                throw new ValueError(this.blame + "->" + ex.getMessage());
            }
        }
    }

}
