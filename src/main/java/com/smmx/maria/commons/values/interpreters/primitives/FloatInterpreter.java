/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.values.interpreters.primitives;

import com.smmx.maria.commons.values.interpreters.Interpreter;
import com.smmx.maria.commons.values.interpreters.InterpreterException;

import java.util.Objects;

import static com.smmx.maria.commons.MariaCommons.sf;

/**
 * @author Osvaldo Miguel Colin
 */
public class FloatInterpreter extends Interpreter<Float> {

    public FloatInterpreter() {
        super(Float.class);
    }

    @Override
    public Float apply(Object value) {
        // NULL CHECK
        if (value == null) {
            return null;
        }

        // TYPE CHECK
        if (value instanceof Float) {
            return (Float) value;
        }

        // VALUEOF
        if (value instanceof Number) {
            return ((Number) value).floatValue();
        } else if (value instanceof String) {
            try {
                return Float.valueOf((String) value);
            } catch (NumberFormatException ex) {
                // NOT A NUMBER
            }
        }

        throw new InterpreterException(sf("\"%s\" can't be converted to a float", Objects.toString(value)));
    }
}
