/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.values.interpreters;

import com.smmx.maria.commons.collections.utils.MapUtils;

import java.util.Map;

/**
 * @author omiguelc
 */
public class InterpreterUtils {

    public static final Map<Class<?>, Class<?>> PRIMITIVES = MapUtils.of(
        byte.class, Byte.class,
        boolean.class, Boolean.class,
        char.class, Character.class,
        int.class, Integer.class,
        float.class, Float.class,
        double.class, Double.class,
        long.class, Long.class
    );

    public static Class<?> normalizeClass(Class<?> clazz) {
        return PRIMITIVES.getOrDefault(clazz, clazz);
    }

}
