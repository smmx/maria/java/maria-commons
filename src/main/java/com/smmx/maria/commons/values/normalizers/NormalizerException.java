/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.values.normalizers;

/**
 * @author Osvaldo Miguel Colin
 */
public class NormalizerException extends RuntimeException {

    public NormalizerException(String msg) {
        super(msg);
    }

}
