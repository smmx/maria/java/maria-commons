/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.values.interpreters.primitives;

import com.smmx.maria.commons.pockets.Pocket;
import com.smmx.maria.commons.values.interpreters.Interpreter;
import com.smmx.maria.commons.values.interpreters.InterpreterException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import static com.smmx.maria.commons.MariaCommons.sf;

/**
 * @author Osvaldo Miguel Colin
 */
public class DateInterpreter extends Interpreter<Date> {

    private final List<SimpleDateFormat> date_formats;
    private final String valid_formats;

    public DateInterpreter(List<String> date_formats) {
        super(Date.class);

        // REGISTER DATE FORMATS
        this.date_formats = new ArrayList<>();

        date_formats.forEach((date_format) -> {
            this.date_formats.add(
                new SimpleDateFormat(date_format)
            );
        });

        valid_formats = String.join(", ", date_formats);
    }

    @Override
    public Date apply(Object value) {
        // NULL CHECK
        if (value == null) {
            return null;
        }

        // TYPE CHECK
        if (value instanceof Date) {
            return (Date) value;
        }

        // VALUEOF
        if (value instanceof String) {
            String value_str = (String) value;

            // PARSE
            Date parsed_date;
            Pocket<Exception> exception_pocket = new Pocket<>();

            // TRY DATE FORMATS
            for (SimpleDateFormat date_format : date_formats) {
                parsed_date = tryParse(value_str, date_format, exception_pocket);

                if (parsed_date != null) {
                    return parsed_date;
                }
            }

            // THROW EXCEPTION
            throw new InterpreterException(sf(
                "%s. Valid formats: [%s]",
                exception_pocket.get().getMessage(),
                valid_formats
            ));
        } else if (value instanceof Number) {
            long millis = ((Number) value).longValue();
            return new Date(millis);
        }

        throw new InterpreterException(sf("\"%s\" can't be converted to a date", Objects.toString(value)));
    }

    private Date tryParse(String value, SimpleDateFormat date_format, Pocket<Exception> exception_pocket) {
        synchronized (date_format) {
            try {
                return date_format.parse(value);
            } catch (ParseException ex) {
                if (exception_pocket.isEmpty()) {
                    exception_pocket.set(ex);
                }
            }
        }

        return null;
    }

}
