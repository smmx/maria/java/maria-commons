/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.values.interpreters;

import com.smmx.maria.commons.values.interpreters.primitives.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

import static com.smmx.maria.commons.collections.utils.ListUtils.of;

/**
 * @author Osvaldo Miguel Colin
 */
public class InterpreterManager {

    // STATIC
    private static InterpreterManager INSTANCE;

    static {
        INSTANCE = null;
    }

    public static InterpreterManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new InterpreterManager();
        }

        return INSTANCE;
    }

    // CLASS
    private final Map<Class<?>, Interpreter> datatypes;

    private InterpreterManager() {
        // INIT
        datatypes = new HashMap<>();

        // REGISTER
        List<String> formats = of(
            "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'z",
            "yyyy-MM-dd'T'HH:mm:ss.SSS",
            "yyyy-MM-dd'T'HH:mm:ss'Z'z",
            "yyyy-MM-dd'T'HH:mm:ss",
            "yyyy-MM-dd'Z'z",
            "yyyy-MM-dd HH:mm:ss.SSS z",
            "yyyy-MM-dd HH:mm:ss.SSS",
            "yyyy-MM-dd HH:mm:ss z",
            "yyyy-MM-dd HH:mm:ss",
            "yyyy-MM-dd z",
            "yyyy-MM-dd",
            "yyyy.MM.dd'T'HH:mm:ss.SSS'Z'z",
            "yyyy.MM.dd'T'HH:mm:ss.SSS",
            "yyyy.MM.dd'T'HH:mm:ss'Z'z",
            "yyyy.MM.dd'T'HH:mm:ss",
            "yyyy.MM.dd'Z'z",
            "yyyy.MM.dd HH:mm:ss.SSS z",
            "yyyy.MM.dd HH:mm:ss.SSS",
            "yyyy.MM.dd HH:mm:ss z",
            "yyyy.MM.dd HH:mm:ss",
            "yyyy.MM.dd z",
            "yyyy.MM.dd"
        );

        register(new BooleanInterpreter());
        register(new ByteArrayInterpreter());
        register(new IntegerInterpreter());
        register(new FloatInterpreter());
        register(new DoubleInterpreter());
        register(new LongInterpreter());
        register(new BigDecimalInterpreter());
        register(new StringInterpreter());
        register(new DateInterpreter(formats));
        register(new ListInterpreter());
        register(new MapInterpreter());
    }

    public void register(Interpreter datatype) {
        datatypes.put(datatype.getClazz(), datatype);
    }

    private <T> Interpreter<T> getInterpreter(Class<T> clazz) {
        Class<?> norm_clazz = InterpreterUtils.normalizeClass(clazz);

        // GET
        Interpreter<T> datatype = datatypes.get(norm_clazz);

        // VALIDATE
        if (datatype == null) {
            throw new NoSuchElementException(clazz.getName());
        }

        return datatype;
    }

    public <T> T valueOf(Object value, Class<T> clazz) {
        return getInterpreter(clazz).apply(value);
    }
}
