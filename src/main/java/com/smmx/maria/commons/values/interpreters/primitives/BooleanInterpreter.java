/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.values.interpreters.primitives;

import com.smmx.maria.commons.values.interpreters.Interpreter;
import org.apache.commons.lang3.BooleanUtils;

/**
 * @author Osvaldo Miguel Colin
 */
public class BooleanInterpreter extends Interpreter<Boolean> {

    public BooleanInterpreter() {
        super(Boolean.class);
    }

    @Override
    public Boolean apply(Object value) {
        // NULL CHECK
        if (value == null) {
            return false;
        }

        // TYPE CHECK
        if (value instanceof Boolean) {
            return (Boolean) value;
        }

        // VALUEOF
        if (value instanceof String) {
            // TRY NUMBER
            try {
                return Double.valueOf((String) value) > 0;
            } catch (NumberFormatException ex) {
                // NOT A NUMBER
            }

            // PARSE STRING
            return BooleanUtils.toBoolean((String) value);
        } else if (value instanceof Number) {
            return ((Number) value).doubleValue() > 0;
        }

        // NOT NULL IS TRUE
        return true;
    }

}
