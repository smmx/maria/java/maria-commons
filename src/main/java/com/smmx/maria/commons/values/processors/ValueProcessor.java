/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.values.processors;

import com.smmx.maria.commons.values.errors.ValueError;
import com.smmx.maria.commons.values.normalizers.NormalizerException;
import com.smmx.maria.commons.values.normalizers.NormalizerManager;
import com.smmx.maria.commons.values.interpreters.InterpreterException;
import com.smmx.maria.commons.values.interpreters.InterpreterManager;
import org.apache.commons.lang3.EnumUtils;
import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

import static com.smmx.maria.commons.MariaCommons.sf;

/**
 * @param <INPUT>
 * @param <OUTPUT>
 * @author Osvaldo Miguel Colin
 */
public class ValueProcessor<INPUT, OUTPUT> implements Function<INPUT, OUTPUT> {

    private final String blame;
    private final Function<INPUT, OUTPUT> parent;

    public ValueProcessor(Function<INPUT, OUTPUT> parent) {
        this(null, parent);
    }

    public ValueProcessor(String blame, Function<INPUT, OUTPUT> parent) {
        // PARENT
        this.parent = parent;

        // BLAME
        this.blame = blame;
    }

    // CORE
    public ValueProcessor<INPUT, OUTPUT> check(Predicate<OUTPUT> predicate, String err_msg) {
        return new ValueProcessor(
            blame,
            (input) -> {
                OUTPUT value = parent.apply((INPUT) input);

                if (!predicate.test(value)) {
                    throw new ValueError(err_msg);
                }

                return value;
            }
        );
    }

    public ValueProcessor<INPUT, OUTPUT> when(Predicate<OUTPUT> predicate, OUTPUT then) {
        return new ValueProcessor(
            blame,
            (input) -> {
                OUTPUT value = parent.apply((INPUT) input);

                if (predicate.test(value)) {
                    return then;
                }

                return value;
            }
        );
    }

    public ValueProcessor<INPUT, OUTPUT> when(Predicate<OUTPUT> predicate, OUTPUT then, OUTPUT or_else) {
        return new ValueProcessor(
            blame,
            (input) -> {
                OUTPUT value = parent.apply((INPUT) input);

                if (predicate.test(value)) {
                    return then;
                }

                return or_else;
            }
        );
    }

    public <NEW_OUTPUT> ValueProcessor<INPUT, NEW_OUTPUT> map(Function<OUTPUT, NEW_OUTPUT> mapper) {
        return new ValueProcessor(
            blame,
            (input) -> {
                OUTPUT value = parent.apply((INPUT) input);

                // RETURN
                return mapper.apply(value);
            }
        );
    }

    public ValueProcessor<INPUT, OUTPUT> peek(Consumer<OUTPUT> consumer) {
        return new ValueProcessor(
            blame,
            (input) -> {
                OUTPUT value = parent.apply((INPUT) input);

                // PEEK
                consumer.accept(value);

                // RETURN
                return value;
            }
        );
    }

    // CHECKS
    public ValueProcessor<INPUT, OUTPUT> options(OUTPUT... options) {
        List options_list = Arrays.asList(options);

        return check(
            value -> {
                return options_list.stream()
                    .anyMatch(an_option -> {
                        return Objects.equals(an_option, value);
                    });
            },
            sf("Input must be one of the following: %s.", options_list.toString())
        );
    }

    public ValueProcessor<INPUT, OUTPUT> notNull() {
        return check(Objects::nonNull, "Input can't be null.");
    }

    public ValueProcessor<INPUT, OUTPUT> ifNull(OUTPUT then) {
        return when(Objects::isNull, then);
    }

    // CAST
    public DictionaryProcessor<INPUT> asDictionary() {
        return new DictionaryProcessor(
            blame,
            (input) -> parent.apply((INPUT) input)
        );
    }

    public ListProcessor<INPUT, Object> asList() {
        return this.asList(Object.class);
    }

    public ListProcessor<INPUT, Map<String, Object>> asListOfDictionaries() {
        return asList((item) -> (Map<String, Object>) item);
    }

    public <LIST_T> ListProcessor<INPUT, LIST_T> asList(Class<LIST_T> clazz) {
        return asList((item) -> (LIST_T) item);
    }

    public <LIST_T> ListProcessor<INPUT, LIST_T> asList(Function<Object, LIST_T> mapper) {
        return new ListProcessor(
            blame,
            (input) -> {
                // CAST VALUE
                List olist = (List) parent.apply((INPUT) input);

                // CREATE NEW LIST
                List<LIST_T> nlist = new ArrayList<>();

                olist.forEach(item -> nlist.add(
                    mapper.apply(item)
                ));

                return nlist;
            }
        );
    }

    public ValueProcessor<INPUT, Object[]> asArray() {
        return this.asArray(Object.class);
    }

    public <ARRAY_T> ValueProcessor<INPUT, ARRAY_T[]> asArray(Class<ARRAY_T> clazz) {
        return map(value -> {
            return (ARRAY_T[]) value;
        });
    }

    public ValueProcessor<INPUT, Boolean> asBoolean() {
        return anyCast(Boolean.class);
    }

    public ValueProcessor<INPUT, String> asString() {
        return anyCast(String.class).map(StringUtils::trimToNull);
    }

    public ValueProcessor<INPUT, Integer> asInteger() {
        return anyCast(Integer.class);
    }

    public ValueProcessor<INPUT, Float> asFloat() {
        return anyCast(Float.class);
    }

    public ValueProcessor<INPUT, Double> asDouble() {
        return anyCast(Double.class);
    }

    public ValueProcessor<INPUT, Long> asLong() {
        return anyCast(Long.class);
    }

    public ValueProcessor<INPUT, BigDecimal> asBigDecimal() {
        return anyCast(BigDecimal.class);
    }

    public ValueProcessor<INPUT, byte[]> asBinary() {
        return anyCast(byte[].class);
    }

    public ValueProcessor<INPUT, Date> asDate() {
        return anyCast(Date.class);
    }

    public <ENUM_T extends Enum<ENUM_T>> ValueProcessor<INPUT, ENUM_T> asEnum(Class<ENUM_T> as_class) {
        return asEnum(as_class, false);
    }

    public <ENUM_T extends Enum<ENUM_T>> ValueProcessor<INPUT, ENUM_T> asEnum(Class<ENUM_T> as_class, boolean case_sensitive) {
        return map(value -> {
            if (value == null) {
                return null;
            }

            if (as_class.isInstance(value)) {
                return (ENUM_T) value;
            }

            if (case_sensitive) {
                return EnumUtils.getEnum(
                    as_class, (String) value
                );
            } else {
                return EnumUtils.getEnumIgnoreCase(
                    as_class, (String) value
                );
            }
        });
    }

    public <NEW_OUTPUT> ValueProcessor<INPUT, NEW_OUTPUT> anyCast(Class<NEW_OUTPUT> clazz) {
        return map(value -> {
            try {
                return InterpreterManager.getInstance()
                    .valueOf(value, clazz);
            } catch (InterpreterException ex) {
                throw new ValueError(sf("Value can't be converted to %s: %s", clazz.getName(), ex.getMessage()));
            }
        });
    }

    public <NEW_OUTPUT> ValueProcessor<INPUT, NEW_OUTPUT> staticCast(Class<NEW_OUTPUT> clazz) {
        return map(value -> {
            // CHECKS
            if (value == null) {
                return null;
            }

            // CAST
            try {
                return (NEW_OUTPUT) value;
            } catch (ClassCastException ex) {
                throw new ValueError(sf("Value can't be cast to type %s: %s", clazz.getName(), ex.getMessage()));
            }
        });
    }

    public ValueProcessor<INPUT, OUTPUT> normalize(String normalizer) {
        return map(value -> {
            try {
                return NormalizerManager.getInstance()
                    .normalize(value, normalizer);
            } catch (NormalizerException ex) {
                throw new ValueError(sf("Value can't be normalized by %s: %s", normalizer, ex.getMessage()));
            }
        });
    }

    // IMPLEMENT
    @Override
    public OUTPUT apply(INPUT input) {
        try {
            return parent.apply(input);
        } catch (Throwable ex) {
            if (this.blame == null) {
                throw new ValueError(ex.getMessage());
            } else {
                throw new ValueError(this.blame + "->" + ex.getMessage());
            }
        }
    }

}
