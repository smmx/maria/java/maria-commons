/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.values.interpreters.primitives;

import com.smmx.maria.commons.values.interpreters.Interpreter;
import com.smmx.maria.commons.values.interpreters.InterpreterException;

import java.math.BigDecimal;
import java.util.Objects;

import static com.smmx.maria.commons.MariaCommons.sf;

/**
 * @author Osvaldo Miguel Colin
 */
public class BigDecimalInterpreter extends Interpreter<BigDecimal> {

    public BigDecimalInterpreter() {
        super(BigDecimal.class);
    }

    @Override
    public BigDecimal apply(Object value) {
        // NULL CHECK
        if (value == null) {
            return null;
        }

        // TYPE CHECK
        if (value instanceof BigDecimal) {
            return (BigDecimal) value;
        }

        // VALUEOF
        if (value instanceof Number) {
            return new BigDecimal(((Number) value).doubleValue());
        } else if (value instanceof String) {
            try {
                return new BigDecimal((String) value);
            } catch (NumberFormatException ex) {
                // NOT A NUMBER
            }
        }

        throw new InterpreterException(sf("\"%s\" can't be converted to decimal.", Objects.toString(value)));
    }

}
