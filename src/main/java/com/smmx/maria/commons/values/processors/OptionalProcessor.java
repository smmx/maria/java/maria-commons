/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.values.processors;

import com.smmx.maria.commons.optional.OptionalValue;
import com.smmx.maria.commons.values.errors.ValueError;

import java.util.function.Function;

/**
 * @param <INPUT>
 * @param <OUTPUT>
 * @author omiguelc
 */
public class OptionalProcessor<INPUT, OUTPUT> implements Function<INPUT, OptionalValue<OUTPUT>> {

    private final String blame;
    private final Function<INPUT, OptionalValue<OUTPUT>> parent;

    public OptionalProcessor(Function<INPUT, OptionalValue<OUTPUT>> parent) {
        this(null, parent);
    }

    public OptionalProcessor(String blame, Function<INPUT, OptionalValue<OUTPUT>> parent) {
        // PARENT
        this.parent = parent;

        // BLAME
        this.blame = blame;
    }

    @Override
    public OptionalValue<OUTPUT> apply(INPUT input) {
        try {
            return parent.apply(input);
        } catch (Throwable ex) {
            if (this.blame == null) {
                throw new ValueError(ex.getMessage());
            } else {
                throw new ValueError(this.blame + "->" + ex.getMessage());
            }
        }
    }

}
