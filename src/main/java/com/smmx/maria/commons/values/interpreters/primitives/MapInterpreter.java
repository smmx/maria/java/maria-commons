/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.values.interpreters.primitives;

import com.smmx.maria.commons.values.interpreters.Interpreter;
import com.smmx.maria.commons.values.interpreters.InterpreterException;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import java.util.Map;
import java.util.Objects;

import static com.smmx.maria.commons.MariaCommons.sf;

/**
 * @author Osvaldo Miguel Colin
 */
public class MapInterpreter extends Interpreter<Map> {

    public MapInterpreter() {
        super(Map.class);
    }

    @Override
    public Map apply(Object value) {
        // NULL CHECK
        if (value == null) {
            return null;
        }

        // TYPE CHECK
        if (value instanceof Map) {
            return (Map) value;
        }

        // VALUEOF
        if (value instanceof String) {
            String str_value = StringUtils.trimToNull((String) value);

            if (str_value == null) {
                return null;
            }

            return new JSONObject(str_value).toMap();
        }

        throw new InterpreterException(sf("\"%s\" can't be converted to map.", Objects.toString(value)));
    }

}
