/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.values.interpreters;

import java.util.function.Function;

/**
 * @param <T>
 * @author Osvaldo Miguel Colin
 */
public abstract class Interpreter<T> implements Function<Object, T> {

    private final Class<T> clazz;

    public Interpreter(Class<T> clazz) {
        this.clazz = clazz;
    }

    public Class<T> getClazz() {
        return clazz;
    }

    public abstract T apply(Object value);

}
