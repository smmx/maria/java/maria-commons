/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.values.interpreters;

/**
 * @author Osvaldo Miguel Colin
 */
public class InterpreterException extends RuntimeException {

    public InterpreterException(String msg) {
        super(msg);
    }

}
