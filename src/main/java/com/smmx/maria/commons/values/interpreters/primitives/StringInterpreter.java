/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.values.interpreters.primitives;

import com.smmx.maria.commons.values.interpreters.Interpreter;
import org.apache.commons.lang3.StringUtils;

/**
 * @author Osvaldo Miguel Colin
 */
public class StringInterpreter extends Interpreter<String> {

    public StringInterpreter() {
        super(String.class);
    }

    @Override
    public String apply(Object value) {
        // NULL CHECK
        if (value == null) {
            return null;
        }

        // TYPE CHECK
        if (value instanceof String) {
            return StringUtils.trimToNull((String) value);
        }

        // VALUEOF
        return value.toString();
    }
}
