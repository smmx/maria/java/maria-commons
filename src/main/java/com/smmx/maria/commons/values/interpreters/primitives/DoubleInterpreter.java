/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.values.interpreters.primitives;

import com.smmx.maria.commons.values.interpreters.Interpreter;
import com.smmx.maria.commons.values.interpreters.InterpreterException;

import java.util.Objects;

import static com.smmx.maria.commons.MariaCommons.sf;

/**
 * @author Osvaldo Miguel Colin
 */
public class DoubleInterpreter extends Interpreter<Double> {

    public DoubleInterpreter() {
        super(Double.class);
    }

    @Override
    public Double apply(Object value) {
        // NULL CHECK
        if (value == null) {
            return null;
        }

        // TYPE CHECK
        if (value instanceof Double) {
            return (Double) value;
        }

        // VALUEOF
        if (value instanceof Number) {
            return ((Number) value).doubleValue();
        } else if (value instanceof String) {
            try {
                return Double.valueOf((String) value);
            } catch (NumberFormatException ex) {
                // NOT A NUMBER
            }
        }

        throw new InterpreterException(sf("\"%s\" can't be converted to a double.", Objects.toString(value)));
    }
}
