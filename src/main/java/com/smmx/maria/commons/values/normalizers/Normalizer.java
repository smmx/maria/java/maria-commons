/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.values.normalizers;

import java.util.function.Function;

/**
 * @param <T>
 * @author Osvaldo Miguel Colin
 */
public abstract class Normalizer<T> implements Function<T, T> {

    private final String name;

    public Normalizer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public abstract T apply(T value);

}
