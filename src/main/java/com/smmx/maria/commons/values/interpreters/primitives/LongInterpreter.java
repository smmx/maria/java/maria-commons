/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.values.interpreters.primitives;

import com.smmx.maria.commons.values.interpreters.Interpreter;
import com.smmx.maria.commons.values.interpreters.InterpreterException;

import java.util.Objects;

import static com.smmx.maria.commons.MariaCommons.sf;

/**
 * @author Osvaldo Miguel Colin
 */
public class LongInterpreter extends Interpreter<Long> {

    public LongInterpreter() {
        super(Long.class);
    }

    @Override
    public Long apply(Object value) {
        // NULL CHECK
        if (value == null) {
            return null;
        }

        // TYPE CHECK
        if (value instanceof Long) {
            return (Long) value;
        }

        // VALUEOF
        if (value instanceof Number) {
            return ((Number) value).longValue();
        } else if (value instanceof String) {
            try {
                return Long.valueOf((String) value);
            } catch (NumberFormatException ex) {
                // NOT A NUMBER
            }
        }

        throw new InterpreterException(sf("\"%s\" can't be converted to a long", Objects.toString(value)));
    }
}
