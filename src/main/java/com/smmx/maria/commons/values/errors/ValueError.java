/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.values.errors;

/**
 * @author Osvaldo Miguel Colin
 */
public class ValueError extends RuntimeException {

    public ValueError(String msg) {
        super(msg);
    }

    public ValueError(Throwable thrwbl) {
        super(thrwbl);
    }

}
