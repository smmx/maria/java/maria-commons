/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.values.normalizers;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;

/**
 * @author Osvaldo Miguel Colin
 */
public class NormalizerManager {

    // STATIC
    private static NormalizerManager INSTANCE;

    static {
        INSTANCE = null;
    }

    public static NormalizerManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new NormalizerManager();
        }

        return INSTANCE;
    }

    // CLASS
    private final Map<String, Normalizer> normalizer;

    private NormalizerManager() {
        // INIT
        normalizer = new HashMap<>();
    }

    public void register(Normalizer normalizer) {
        this.normalizer.put(normalizer.getName(), normalizer);
    }

    private <T> Normalizer<T> getNormalizer(String name) {
        // GET
        Normalizer<T> normalizer = this.normalizer.get(name);

        // VALIDATE
        if (normalizer == null) {
            throw new NoSuchElementException(name);
        }

        return normalizer;
    }

    public <T> T normalize(T value, String normalizer) {
        return (T) getNormalizer(normalizer).apply(value);
    }
}
