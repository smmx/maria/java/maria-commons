/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.collections.utils;

import com.smmx.maria.commons.optional.UndefinedValue;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

/**
 * @author Osvaldo Miguel Colin
 */
public class ListUtils {

    public static <T> List<T> merge(List<T>... lists) {
        List<T> merged_list = new ArrayList<>();

        for (List<T> append_list : lists) {
            if (append_list != null) {
                merged_list.addAll(append_list);
            }
        }

        return merged_list;
    }

    public static <T> List<T> of(T... items) {
        return of(Collections.emptyList(), items);
    }

    public static <T> List<T> of(List<T> base, T... items) {
        List list = new ArrayList<>();

        list.addAll(base);

        for (T item : items) {
            boolean defined = !Objects.equals(
                item, UndefinedValue.getInstance()
            );

            if (defined) {
                list.add(item);
            }
        }

        return list;
    }

}
