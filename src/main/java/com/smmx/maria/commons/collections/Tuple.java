package com.smmx.maria.commons.collections;

public class Tuple {

    private final Object[] args;

    public Tuple(Object... args) {
        this.args = args;
    }

    public <T> T get(int pos, Class<T> clazz) {
        Object value = args[pos];

        // CHECKS
        if (value == null) {
            return null;
        }

        // CAST
        return (T) value;
    }

}
