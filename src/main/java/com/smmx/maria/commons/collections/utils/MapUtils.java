/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.collections.utils;

import com.smmx.maria.commons.optional.UndefinedValue;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;

/**
 * @author Osvaldo Miguel Colin
 */
public class MapUtils {

    public static <K, V> Map<K, V> remap(Map<K, V> map, Map<K, K> dictionary) {
        Map<K, V> nmap = new HashMap<>();

        map.forEach((key, value) -> {
            if (dictionary.containsKey(key)) {
                key = dictionary.get(key);
                nmap.put(key, value);
            }
        });

        return nmap;
    }

    public static <K, V> Map<K, V> remap(Map<K, V> map, Function<K, K> mapper) {
        return remap(map, mapper, true);
    }

    public static <K, V> Map<K, V> remap(Map<K, V> map, Function<K, K> mapper, boolean ignore_null_keys) {
        Map<K, V> nmap = new HashMap<>();

        map.forEach((key, value) -> {
            key = mapper.apply(key);

            if (!ignore_null_keys || key != null) {
                nmap.put(key, value);
            }
        });

        return nmap;
    }

    public static <K, V> Map<K, V> merge(Map<K, V>... maps) {
        Map<K, V> result = new HashMap<>();

        for (Map<K, V> append_map : maps) {
            if (append_map != null) {
                result.putAll(append_map);
            }
        }

        return result;
    }

    public static <K, V> Map<K, V> of(Object... items) {
        return of(Collections.emptyMap(), items);
    }

    public static <K, V> Map<K, V> of(Map<K, V> base, Object... items) {
        if (items == null) {
            return new HashMap<>();
        }

        K key = null;
        V value = null;
        Map<K, V> map = new HashMap(base);

        for (int i = 0; i < items.length; i++) {
            if ((i % 2) == 0) {
                key = (K) items[i];
            } else {
                value = (V) items[i];

                boolean defined = !Objects.equals(
                    value, UndefinedValue.getInstance()
                );

                if (defined) {
                    map.put(key, value);
                }

                key = null;
                value = null;
            }
        }

        if (key != null) {
            map.put(key, value);
        }

        return map;
    }

}
