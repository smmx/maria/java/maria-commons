/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.time;

import java.util.Calendar;

/**
 * @author omiguelc
 */
public class CalendarSteppers {

    public static Calendar nextStartOfWeek(Calendar cal) {
        Calendar ncal = CalendarUtils.clone(cal);

        // SET HMSM = 0
        ncal.set(Calendar.HOUR_OF_DAY, 0);
        ncal.set(Calendar.MINUTE, 0);
        ncal.set(Calendar.SECOND, 0);
        ncal.set(Calendar.MILLISECOND, 0);

        // INCREASE 7 DAYS
        ncal.add(Calendar.DATE, 7);

        return ncal;
    }

    public static Calendar nextStartOfDay(Calendar cal) {
        Calendar ncal = CalendarUtils.clone(cal);

        // SET HMSM = 0
        ncal.set(Calendar.HOUR_OF_DAY, 0);
        ncal.set(Calendar.MINUTE, 0);
        ncal.set(Calendar.SECOND, 0);
        ncal.set(Calendar.MILLISECOND, 0);

        // INCREASE 1 DAY
        ncal.add(Calendar.DATE, 1);

        return ncal;
    }

    public static Calendar nextStartOfHour(Calendar cal) {
        Calendar ncal = CalendarUtils.clone(cal);

        // SET MSM = 0
        ncal.set(Calendar.MINUTE, 0);
        ncal.set(Calendar.SECOND, 0);
        ncal.set(Calendar.MILLISECOND, 0);

        // INCREASE 1 HOUR
        ncal.add(Calendar.HOUR_OF_DAY, 1);

        return ncal;
    }

}
