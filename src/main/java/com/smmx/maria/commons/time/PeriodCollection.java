package com.smmx.maria.commons.time;

import java.util.*;

public class PeriodCollection implements Iterable<Period> {

    private final Calendar begin;
    private final Calendar end;

    private final CalendarStepper stepper;

    private int size;

    public PeriodCollection(Date begin, Date end, CalendarStepper stepper) {
        if (end.before(begin)) {
            throw new IllegalArgumentException("\"Begin\" has to be before \"End\"");
        }

        this.begin = Calendar.getInstance();
        this.end = Calendar.getInstance();
        this.begin.setTime(begin);
        this.end.setTime(end);

        this.stepper = stepper;

        this.size = -1;
    }

    public Date getBegin() {
        return begin.getTime();
    }

    public Date getEnd() {
        return end.getTime();
    }

    public int size() {
        // SHORT-CIRCUIT
        if (size > -1) {
            return size;
        }

        // COUNT
        size = 0;

        for (Period ignored : this) {
            size++;
        }

        return size;
    }

    public List<Period> toList() {
        List<Period> periods = new ArrayList<>();

        for (Period period : this) {
            periods.add(period);
        }

        return periods;
    }

    @Override
    public Iterator<Period> iterator() {
        return CalendarUtils.periodIterator(
            begin, end, stepper
        );
    }
}
