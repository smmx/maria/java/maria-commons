/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.time;

import java.util.Calendar;

/**
 * @author omiguelc
 */
public interface CalendarStepper {

    Calendar apply(Calendar start);

}
