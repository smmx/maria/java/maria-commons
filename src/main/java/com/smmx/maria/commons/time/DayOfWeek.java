/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.time;

import java.util.Calendar;

/**
 * @author omiguelc
 */
public enum DayOfWeek {
    MONDAY(Calendar.MONDAY),
    TUESDAY(Calendar.TUESDAY),
    WEDNESDAY(Calendar.WEDNESDAY),
    THURSDAY(Calendar.THURSDAY),
    FRIDAY(Calendar.FRIDAY),
    SATURDAY(Calendar.SATURDAY),
    SUNDAY(Calendar.SUNDAY);

    private final int day_of_week;

    DayOfWeek(int day_of_week) {
        this.day_of_week = day_of_week;
    }

    public int toInt() {
        return day_of_week;
    }

}
