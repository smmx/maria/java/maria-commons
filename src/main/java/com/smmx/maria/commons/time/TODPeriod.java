package com.smmx.maria.commons.time;


import java.util.*;

public class TODPeriod {

    // STATIC
    private static TODPeriod getTimePeriod(Period period) {
        int begin = DateUtils.getTimeOfDay(period.getBegin());
        int end = DateUtils.getTimeOfDay(period.getEnd());
        return new TODPeriod(begin, end);
    }

    // MEMBER
    private final int begin;
    private final int end;

    public TODPeriod(int begin, int end) {
        this.begin = begin;
        this.end = end;
    }

    public int getBegin() {
        return begin;
    }

    public int getEnd() {
        return end;
    }

    public int getDuration() {
        return end - begin;
    }

    public TODPeriod getOverlap(TODPeriod period) {
        int begin_seconds = begin;
        int end_seconds = end;

        int period_begin_seconds = period.begin;
        int period_end_seconds = period.end;

        boolean overlaps = (begin_seconds <= period_begin_seconds && period_begin_seconds <= end_seconds)
            || (begin_seconds <= period_end_seconds && period_end_seconds <= end_seconds)
            || (period_begin_seconds <= begin_seconds && begin_seconds <= period_end_seconds)
            || (period_begin_seconds <= end_seconds && end_seconds <= period_end_seconds);

        // SHORT-CIRCUIT: DOESN'T OVERLAP
        if (!overlaps) {
            return null;
        }

        int overlap_begin_date;
        int overlap_end_date;

        // STARTING DATE CONSTRAINT
        if (period_begin_seconds < begin_seconds) {
            overlap_begin_date = begin;
        } else {
            overlap_begin_date = period.begin;
        }

        // ENDING DATE CONSTRAINT
        if (end_seconds < period_end_seconds) {
            overlap_end_date = end;
        } else {
            overlap_end_date = period.end;
        }

        // RETURN
        return new TODPeriod(overlap_begin_date, overlap_end_date);
    }

    public List<Period> getOverlaps(Period period) {
        PeriodCollection days = period.split(CalendarSteppers::nextStartOfDay);

        if (days.size() == 0) {
            return Collections.emptyList();
        }

        List<Period> periods = new ArrayList<>();

        for (Period day : days) {
            TODPeriod tod_period = getTimePeriod(day);
            TODPeriod overlap = getOverlap(tod_period);

            // SHORT-CIRCUIT: DOESN'T OVERLAP
            if (overlap == null) {
                continue;
            }

            Date day_begin = day.getBegin();
            Date day_end = day.getEnd();

            day_begin = DateUtils.setTimeOfDay(day_begin, overlap.getBegin());
            day_end = DateUtils.setTimeOfDay(day_end, overlap.getEnd());

            periods.add(new Period(day_begin, day_end));
        }

        return periods;
    }

    public boolean hasOverlap(TODPeriod period) {
        int begin_seconds = begin;
        int end_seconds = end;

        int period_begin_seconds = period.begin;
        int period_end_seconds = period.end;

        return (begin_seconds <= period_begin_seconds && period_begin_seconds <= end_seconds)
            || (begin_seconds <= period_end_seconds && period_end_seconds <= end_seconds)
            || (period_begin_seconds <= begin_seconds && begin_seconds <= period_end_seconds)
            || (period_begin_seconds <= end_seconds && end_seconds <= period_end_seconds);
    }

    public boolean hasOverlap(Period period) {
        PeriodCollection days = period.split(CalendarSteppers::nextStartOfDay);

        if (days.size() == 0) {
            return false;
        }

        for (Period day : days) {
            TODPeriod tod = getTimePeriod(day);

            if (hasOverlap(tod)) {
                return true;
            }
        }

        return false;
    }

    public boolean contains(Date date) {
        int begin_seconds = begin;
        int end_seconds = end;

        int date_seconds = DateUtils.getTimeOfDay(date);

        return (begin_seconds <= date_seconds && date_seconds <= end_seconds);
    }

    public boolean contains(TODPeriod period) {
        int begin_seconds = begin;
        int end_seconds = end;

        int period_begin_seconds = period.getBegin();
        int period_end_seconds = period.getEnd();

        return (begin_seconds <= period_begin_seconds && period_begin_seconds <= end_seconds)
            && (begin_seconds <= period_end_seconds && period_end_seconds <= end_seconds);
    }


}
