/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.time;

import com.smmx.maria.commons.pockets.Pocket;

import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;

/**
 * @author omiguelc
 */
public class CalendarUtils {

    public static Calendar clone(Calendar cal) {
        Date ref_date = cal.getTime();

        Calendar clone = Calendar.getInstance();
        clone.setTime(ref_date);

        return clone;
    }

    public static Calendar toCalendar(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal;
    }

    public static Calendar nextDayOfWeek(Calendar cal, int day_of_week) {
        Calendar clone = clone(cal);

        int this_day_of_week = clone.get(Calendar.DAY_OF_WEEK);
        int diff = (day_of_week - this_day_of_week) % 7;
        clone.add(Calendar.DATE, diff);

        return clone;
    }

    public static Calendar previousDayOfWeek(Calendar cal, int day_of_week) {
        Calendar clone = clone(cal);

        int this_day_of_week = clone.get(Calendar.DAY_OF_WEEK);
        int diff = (this_day_of_week - day_of_week) % 7;
        clone.add(Calendar.DATE, -diff);

        return clone;
    }

    public static Calendar toStartOfDay(Calendar cal) {
        Calendar clone = clone(cal);

        clone.set(Calendar.HOUR_OF_DAY, 0);
        clone.set(Calendar.MINUTE, 0);
        clone.set(Calendar.SECOND, 0);
        clone.set(Calendar.MILLISECOND, 0);

        return clone;
    }

    public static Calendar toEndOfDay(Calendar cal) {
        Calendar ncal = clone(cal);

        ncal.set(Calendar.HOUR_OF_DAY, 23);
        ncal.set(Calendar.MINUTE, 59);
        ncal.set(Calendar.SECOND, 59);
        ncal.set(Calendar.MILLISECOND, 999);

        return ncal;
    }

    public static Iterator<Calendar> calendarIterator(Calendar start, CalendarStepper step) {
        Pocket<Calendar> calendar_pocket = new Pocket<>(start);

        return new Iterator<Calendar>() {
            private boolean first = true;

            @Override
            public boolean hasNext() {
                return true;
            }

            @Override
            public Calendar next() {
                Calendar current = calendar_pocket.get();

                if (first) {
                    first = false;
                    return current;
                }

                // STEP
                current = step.apply(current);

                // UPDATE REFERENCE
                calendar_pocket.set(current);

                // RETURN
                return current;
            }

        };
    }

    public static Iterator<Period> periodIterator(Calendar start, Calendar stop, CalendarStepper step) {
        Iterator<Calendar> left_iterator = calendarIterator(start, step);
        Iterator<Calendar> right_iterator = calendarIterator(start, step);

        // FORWARD THE RIGHT BY ONE
        right_iterator.next();

        // INIT
        Calendar init_next_left = CalendarUtils.clone(start);

        return new Iterator<Period>() {

            private Calendar next_left = init_next_left;

            @Override
            public boolean hasNext() {
                return next_left.before(stop);
            }

            @Override
            public Period next() {
                Calendar nleft = left_iterator.next();
                Calendar nright = right_iterator.next();

                if (nright.after(stop)) {
                    nright = CalendarUtils.clone(stop);
                }

                next_left = nright;

                return new Period(
                    nleft.getTime(),
                    nright.getTime()
                );
            }
        };
    }
}
