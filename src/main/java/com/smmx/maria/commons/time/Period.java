/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.time;

import java.util.Date;

/**
 * @author omiguelc
 */
public class Period {

    private final Date begin;
    private final Date end;

    public Period(Date begin, Date end) {
        if (end.before(begin)) {
            throw new IllegalArgumentException("\"Begin\" has to be before \"End\"");
        }

        this.begin = begin;
        this.end = end;
    }

    public PeriodCollection split(CalendarStepper stepper) {
        return new PeriodCollection(begin, end, stepper);
    }

    public Date getBegin() {
        return begin;
    }

    public Date getEnd() {
        return end;
    }

    public long getDuration() {
        return end.getTime() - begin.getTime();
    }

    public long getDurationSeconds() {
        return (end.getTime() - begin.getTime()) / 1000L;
    }

    public Period getOverlap(Period a_period) {
        long begin_millis = begin.getTime();
        long end_millis = end.getTime();

        long a_period_begin_millis = a_period.begin.getTime();
        long a_period_end_millis = a_period.end.getTime();

        boolean overlap = (begin_millis <= a_period_begin_millis && a_period_begin_millis <= end_millis)
            || (begin_millis <= a_period_end_millis && a_period_end_millis <= end_millis)
            || (a_period_begin_millis <= begin_millis && begin_millis <= a_period_end_millis)
            || (a_period_begin_millis <= end_millis && end_millis <= a_period_end_millis);

        if (!overlap) {
            return null;
        }

        Date overlap_begin_date;
        Date overlap_end_date;

        // STARTING DATE CONSTRAINT
        if (a_period_begin_millis < begin_millis) {
            overlap_begin_date = begin;
        } else {
            overlap_begin_date = a_period.begin;
        }

        // ENDING DATE CONSTRAINT
        if (end_millis < a_period_end_millis) {
            overlap_end_date = end;
        } else {
            overlap_end_date = a_period.end;
        }

        // RETURN
        return new Period(overlap_begin_date, overlap_end_date);
    }

    public boolean contains(Date adate) {
        long begin_millis = begin.getTime();
        long end_millis = end.getTime();

        long adate_millis = adate.getTime();

        return (begin_millis <= adate_millis && adate_millis <= end_millis);
    }

    public boolean contains(Period aperiod) {
        long begin_millis = begin.getTime();
        long end_millis = end.getTime();

        long aperiod_begin_millis = aperiod.getBegin().getTime();
        long aperiod_end_millis = aperiod.getEnd().getTime();

        return (begin_millis <= aperiod_begin_millis && aperiod_begin_millis <= end_millis)
            && (begin_millis <= aperiod_end_millis && aperiod_end_millis <= end_millis);
    }

    public boolean overlaps(Period aperiod) {
        long begin_millis = begin.getTime();
        long end_millis = end.getTime();

        long aperiod_begin_millis = aperiod.begin.getTime();
        long aperiod_end_millis = aperiod.end.getTime();

        return (begin_millis <= aperiod_begin_millis && aperiod_begin_millis <= end_millis)
            || (begin_millis <= aperiod_end_millis && aperiod_end_millis <= end_millis)
            || (aperiod_begin_millis <= begin_millis && begin_millis <= aperiod_end_millis)
            || (aperiod_begin_millis <= end_millis && end_millis <= aperiod_end_millis);
    }
}
