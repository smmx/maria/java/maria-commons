package com.smmx.maria.commons.time;

import java.util.Calendar;
import java.util.Date;

public class DateUtils {

    public static int getTimeOfDay(Date date) {
        int time = 0;

        Calendar cal = CalendarUtils.toCalendar(date);
        time += cal.get(Calendar.HOUR_OF_DAY) * 3600;
        time += cal.get(Calendar.MINUTE) * 60;
        time += cal.get(Calendar.SECOND);

        return time;
    }

    public static Date setTimeOfDay(Date date, int time) {
        int seconds = (int) Math.floor(time % 60);
        int minutes = (int) Math.floor((time % 3600) / 60d);
        int hours = (int) Math.floor((time % 86400) / 3600d);

        Calendar cal = CalendarUtils.toCalendar(date);
        cal.set(Calendar.HOUR_OF_DAY, hours);
        cal.set(Calendar.MINUTE, minutes);
        cal.set(Calendar.SECOND, seconds);

        return cal.getTime();
    }

}
