/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.boot;

import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.PropertyConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.MalformedURLException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.TimeZone;

import static com.smmx.maria.commons.MariaCommons.path;
import static com.smmx.maria.commons.MariaCommons.vp;

/**
 * @author Osvaldo Miguel Colin
 */
public class AppRuntime {

    // SINGLETON
    private static AppRuntime INSTANCE;

    static {
        INSTANCE = null;
    }

    public static AppRuntime getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new AppRuntime();
        }

        return INSTANCE;
    }

    // MEMBER
    private Logger logger;

    private App app;
    private Path home_directory;

    private AppRuntime() {
    }

    public App app() {
        return app;
    }

    public Path home() {
        return home_directory;
    }

    public AppRuntime headless() {
        System.setProperty("java.awt.headless", "true");
        return this;
    }

    public AppRuntime timezone(String tz) {
        TimeZone time_zone_default = TimeZone.getTimeZone(tz);
        TimeZone.setDefault(time_zone_default);

        System.setProperty("user.timezone", tz);

        return this;
    }

    public void run(App app, String[] args) {
        ////////////////////////////////////////////////////////////////////////
        // INIT ////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        this.app = app;

        ////////////////////////////////////////////////////////////////////////
        // ARG PARSE ///////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        ArgumentParser parser = ArgumentParsers.newFor(app.getName())
            .build()
            .defaultHelp(true)
            .description(app.getDescription());

        parser.addArgument("-d", "--home-directory")
            .metavar("Home Directory")
            .help("Home Directory which contains the app.json file and more config files if they exist.")
            .dest("home_directory")
            .type(String.class)
            .required(true);

        parser.addArgument("-l", "--log")
            .metavar("Log File")
            .help("Log file for the app.")
            .dest("log")
            .type(String.class)
            .required(false);

        app.configureArgumentParser(parser);

        // PARSE ARGUMENTS
        Namespace namespace = null;

        try {
            namespace = parser.parseArgs(args);
        } catch (ArgumentParserException ex) {
            // HANDLE
            parser.handleError(ex);
        }

        // SHORT-CIRCUIT
        if (namespace == null) {
            throw new AppBootException("Bad Arguments.");
        }

        ////////////////////////////////////////////////////////////////////////
        // HOME DIR ////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        String arg_home_directory = vp()
            .asString()
            .map(StringUtils::trimToEmpty)
            .apply(namespace.getString("home_directory"));

        this.home_directory = FileSystems.getDefault().getPath(
            arg_home_directory
        );

        ////////////////////////////////////////////////////////////////////////
        // LOGGER //////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        String arg_log = vp()
            .asString()
            .ifNull("log/app.log")
            .apply(namespace.getString("log"));

        // INIT LOG
        if (arg_log.startsWith("/")) {
            System.setProperty("maria.app.log", arg_log);
        } else {
            System.setProperty("maria.app.log", path(arg_home_directory, arg_log));
        }

        try {
            PropertyConfigurator.configure(
                home_directory.resolve("config/log.properties")
                    .toUri()
                    .toURL()
            );
        } catch (MalformedURLException ex) {
            throw new AppBootException(ex);
        }

        ////////////////////////////////////////////////////////////////////////
        // LOG /////////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        AppConfiguration.getInstance().log();

        ////////////////////////////////////////////////////////////////////////
        // SEQUENCE ////////////////////////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////
        //
        // DO NOT MAKE LOGGER STATIC AS IT REQUIRES THE PREVIOUS LINES TO RUN FIRST
        // BEFORE LOGGING IS ALLOWED
        Logger logger = LoggerFactory.getLogger(AppRuntime.class);

        // BEGIN SEQUENCE
        boolean sequence_ok = true;

        if (sequence_ok) {
            try {
                logger.info("Configure");
                app.configure(namespace.getAttrs());
            } catch (AppConfigurationException ex) {
                logger.error("Failed to configure.", ex);
                sequence_ok = true;
            }
        }

        if (sequence_ok) {
            try {
                logger.info("Setup");
                app.setup();
            } catch (AppSetupException ex) {
                logger.error("Failed to setup.", ex);
                sequence_ok = true;
            }
        }

        if (sequence_ok) {
            logger.info("Start");
            app.start();
        }
        // END SEQUENCE
    }

    public void fail(int code, String reason) {
        logger.error("Application has failed: {}.", reason);
        this.exit(code);
    }

    public void fail(int code, Throwable ex) {
        logger.error("Application has failed.", ex);
        this.exit(code);
    }

    public void exit(int code) {
        logger.info("Exiting with exit code {}.", code);
        System.exit(code);
    }

}
