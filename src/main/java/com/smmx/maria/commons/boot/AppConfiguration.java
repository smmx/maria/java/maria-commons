/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.boot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static com.smmx.maria.commons.MariaCommons.dp;

/**
 * @author Osvaldo Miguel Colin
 */
public class AppConfiguration extends HashMap<String, Object> {

    // SINGLETON
    private static AppConfiguration INSTANCE;

    static {
        INSTANCE = null;
    }

    public static AppConfiguration getInstance() {
        if (INSTANCE == null) {
            try {
                INSTANCE = new AppConfiguration();
            } catch (IOException ex) {
                throw new AppBootException(ex);
            }
        }

        return INSTANCE;
    }

    // STATIC
    private static final Logger LOGGER = LoggerFactory.getLogger(AppConfiguration.class);

    // MEMBER
    private final String version;
    private final String author;
    private final Date timestamp;

    private AppConfiguration() throws IOException {
        Map<String, Object> configuration = read();

        // LOAD
        this.putAll(configuration);

        // DATA
        this.version = dp()
            .require("version")
            .asString()
            .notNull()
            .apply(this);

        this.author = dp()
            .require("author")
            .asString()
            .notNull()
            .apply(this);

        this.timestamp = dp()
            .require("timestamp")
            .asDate()
            .notNull()
            .apply(this);
    }

    // LOAD
    private Map<String, Object> read() throws IOException {
        File file = AppRuntime.getInstance().home()
            .resolve("config/app.yaml")
            .toFile();

        // READER
        FileReader reader = new FileReader(file);

        // RETURN
        return Collections.unmodifiableMap(
            new Yaml().loadAs(reader, Map.class)
        );
    }

    // GETTERS
    public String getVersion() {
        return version;
    }

    public String getAuthor() {
        return author;
    }

    public Date getTimestamp() {
        return timestamp;
    }

    // METHODS
    public void log() {
        LOGGER.info(
            "Config: Version={}, Author={}, Timestamp={}",
            AppConfiguration.getInstance().getVersion(),
            AppConfiguration.getInstance().getAuthor(),
            AppConfiguration.getInstance().getTimestamp()
        );
    }
}
