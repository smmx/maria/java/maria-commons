/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.smmx.maria.commons.boot;

/**
 * @author Osvaldo Miguel Colin
 */
public class AppBootException extends RuntimeException {

    public AppBootException(String msg) {
        super(msg);
    }

    public AppBootException(Throwable thrwbl) {
        super(thrwbl);
    }

    public AppBootException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

}
