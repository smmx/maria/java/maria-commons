# Functions Available in Maria Commons

```java
// PATH
MariaCommons.path(base_path, path1, ..., pathn) // Allow you to join various paths together

// SQL
MariaCommons.sql(sql) // Creates and instance of SQL Template
MariaCommons.transaction(operation) // Creates a transaction wrapped operation

// ALGOS
MariaCommons.between(value, min, max) // Returns true if the value is withing the specified boundaries
MariaCommons.max(values) // Returns the max value in a collection of values or arguments
MariaCommons.min(values) // Returns the min value in a collection of values or arguments
MariaCommons.ifnull(value, or_else) // Returns the or_else value if the first argument is null
MariaCommons.when(condition, then, or_else) // Performs the same operation of an if statement

// COLLECTIONS
MariaCommons.t(items) // Returns an array containing all the arguments given
MariaCommons.tuple(items) // Returns an array containing all the arguments given
MariaCommons.a(items) // Returns an array containing all the arguments of the same type given
MariaCommons.array(items) // Returns an array containing all the arguments of the same type given
MariaCommons.l(items) // Returns a list containing all the arguments given
MariaCommons.list(items) // Returns a list containing all the arguments given
MariaCommons.d(items) // Returns a map containing all the arguments given. Odd position values are considered keys, and even position values are considered the values
MariaCommons.dictionary(items) // Returns a map containing all the arguments given. Odd position values are considered keys, and even position values are considered the values

// STRINGS
MariaCommons.mlstr(strings) // Returns a joined string of all the string arguments given
MariaCommons.xmlstr(separator, strings) // Returns a joined string of all the string arguments given using the given separator

// VALUES
MariaCommons.vp() // Returns a new ValueProcessor instance
MariaCommons.dp() // Returns a new DictionaryProcessor instance

// DATA
MariaCommons.valueOf(value, class) // Tries to convert the value to the requested class

// DATES
MariaCommons.now() // Returns the current date

// BINARY
MariaCommons.hexToBin(hex) // Returns a byte array representing the hex value given
MariaCommons.binToHex(bin) // Returns a hex representing the byte array given

// UUID
MariaCommons.uuid() // Return a v4 UUID string
MariaCommons.buuid() // Return a v4 UUID byte array
```
